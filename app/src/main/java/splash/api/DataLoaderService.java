package splash.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zpi.mobileshop.BuildConfig;

import java.util.LinkedList;
import java.util.List;

import categories.api.ICategoriesAPI;
import extra.ArrayUtils;
import extra.DateUtils;
import extra.GsonKeyValueConverter;
import extra.Time;
import extra.Triple;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import main.basket.BasketItem;
import products.Product;
import products.api.IProductsAPI;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataLoaderService {
    private final IShopDataAPI shopDataAPI;
    private final ICategoriesAPI categoriesAPI;
    private final IProductsAPI productsAPI;
    private final CompositeDisposable disposable = new CompositeDisposable();

    private final SharedPreferences sharedPreferences;

    public DataLoaderService(Context context) {
        Gson keyValueGson = new GsonBuilder()
                .registerTypeAdapter(List.class, new GsonKeyValueConverter())
                .create();

        shopDataAPI = new Retrofit.Builder()
                .baseUrl(BuildConfig.WebAPIAddress)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(keyValueGson))
                .build()
                .create(IShopDataAPI.class);

        categoriesAPI = new Retrofit.Builder()
                .baseUrl(BuildConfig.WebAPIAddress)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ICategoriesAPI.class);

        Gson productsGson = new GsonBuilder()
                .registerTypeAdapter(Product.class, new Product.ProductTypeAdapter())
                .create();

        productsAPI = new Retrofit.Builder()
                .baseUrl(BuildConfig.WebAPIAddress)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(productsGson))
                .build()
                .create(IProductsAPI.class);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void clearDisposables() {
        disposable.clear();
    }

    private String getLastRefreshDate() {
        return sharedPreferences.getString("LastDataRefreshDate", null);
    }

    private void setLastRefreshDate(String date) {
        sharedPreferences.edit().putString("LastDataRefreshDate", date).apply();
    }

    public void startLoadingData(DataLoadCallback callback) {
        refreshShopData(callback);
    }

    private void refreshShopData(DataLoadCallback callback) {
        disposable.add(
                shopDataAPI
                        .getShopData()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(pairs -> {
                            ShopData data = ShopData.getInstance();

                            for (Pair<String, String> pair : pairs) {
                                switch (pair.first) {
                                    case "name": {
                                        data.setShopName(pair.second);
                                        break;
                                    }

                                    case "address": {
                                        data.setAddress(pair.second);
                                        break;
                                    }

                                    case "additionalInfo": {
                                        data.setAdditionalInfo(pair.second);
                                        break;
                                    }

                                    case "endOrderingTime": {
                                        data.setEndOrderingTime(Time.parseTime(pair.second));
                                        break;
                                    }

                                    case "startCollectingTime": {
                                        data.setStartCollectingTime(Time.parseTime(pair.second));
                                        break;
                                    }

                                    case "endCollectingTime": {
                                        data.setEndCollectingTime(Time.parseTime(pair.second));
                                        break;
                                    }
                                }
                            }

                            refreshCategories(callback);
                        }, throwable -> {
                            Log.e("DataLoaderService", "Error: Can't download shopData from server!\n" + throwable.getMessage());
                            throwable.printStackTrace();
                            callback.dataDownloadFailure();
                        })
        );
    }

    private void refreshCategories(DataLoadCallback callback) {
        disposable.add(
                categoriesAPI
                        .getAllCategories()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                data -> {
                                    data.sortSubcategories();
                                    Realm realm = Realm.getDefaultInstance();
                                    realm.executeTransaction(realmInst -> realmInst.copyToRealmOrUpdate(data));
                                    realm.close();

                                    refreshProducts(callback);
                                },
                                throwable -> {
                                    Log.e("DataLoaderService", "Error: Can't download categories from server!\n" + throwable.getMessage());
                                    throwable.printStackTrace();
                                    callback.dataDownloadFailure();
                                }
                        )
        );
    }

    private void refreshProducts(DataLoadCallback callback) {
        String lastRefreshDate = getLastRefreshDate();
        String nextRefreshDate = DateUtils.getCurrentDateTimeString();

        disposable.add(
                downloadProductsAsync(lastRefreshDate)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(products -> disposable.add(
                                processNewProductsAsync(products)
                                        .subscribeOn(Schedulers.computation())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(basketProductChanges -> {
                                            setLastRefreshDate(nextRefreshDate);

                                            if (basketProductChanges.first.isEmpty() && basketProductChanges.second.isEmpty()) {
                                                callback.dataDownloadSuccess();
                                            } else {
                                                callback.dataDownloadNeedToAcceptChanges(basketProductChanges);
                                            }
                                        }, throwable -> callback.dataDownloadFailure())
                        ), throwable -> callback.dataDownloadFailure())
        );
    }

    private Observable<List<Product>> downloadProductsAsync(String date) {
        if (date == null) {
            return productsAPI.getAllProducts();
        }

        return productsAPI.getAllProducts(date);
    }

    private Observable<Pair<LinkedList<String>, LinkedList<Triple<Product, Double, Double>>>> processNewProductsAsync(List<Product> newProducts) {
        return Observable.fromCallable(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();

            LinkedList<Pair<Long, Double>> basketProductsPrices = new LinkedList<>();
            RealmResults<BasketItem> basketItems = realm.where(BasketItem.class).findAll();

            for (BasketItem basketItem : basketItems) {
                Product product = basketItem.getProduct();
                basketProductsPrices.add(new Pair<>(product.getId(), product.getPrice()));
            }

            realm.copyToRealmOrUpdate(newProducts);

            basketItems = realm.where(BasketItem.class).findAll();
            LinkedList<Long> unavailableBasketProductIds = new LinkedList<>();
            LinkedList<String> unavailableBasketProductNames = new LinkedList<>();
            LinkedList<Triple<Product, Double, Double>> priceChangedProductNames = new LinkedList<>();

            for (BasketItem basketItem : basketItems) {
                Product product = basketItem.getProduct();

                if (product.getStatus() == Product.Status.UNAVAILABLE) {
                    unavailableBasketProductIds.add(product.getId());
                    unavailableBasketProductNames.add(product.getName());
                } else {
                    for (Pair<Long, Double> idAndPrice : basketProductsPrices) {
                        if (idAndPrice.first == product.getId() && idAndPrice.second != product.getPrice()) {
                            priceChangedProductNames.add(
                                    new Triple<>
                                            (new Product(product), idAndPrice.second, product.getPrice())
                            );
                        }
                    }
                }
            }

            realm
                    .where(BasketItem.class)
                    .in("productId", ArrayUtils.convertListToObjectArray(unavailableBasketProductIds))
                    .findAll()
                    .deleteAllFromRealm();

            realm.commitTransaction();
            realm.close();

            return new Pair<>(unavailableBasketProductNames, priceChangedProductNames);
        });
    }
}