package splash.api;

import android.util.Pair;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

interface IShopDataAPI {
    @GET("api/shop/info")
    Observable<List<Pair<String, String>>> getShopData();
}
