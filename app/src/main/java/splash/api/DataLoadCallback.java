package splash.api;

import android.util.Pair;

import java.util.LinkedList;

import extra.Triple;
import products.Product;

public interface DataLoadCallback {
    void dataDownloadSuccess();

    void dataDownloadFailure();

    void dataDownloadNeedToAcceptChanges(Pair<LinkedList<String>, LinkedList<Triple<Product, Double, Double>>> productChanges);
}
