package splash.api;

import extra.Time;

public class ShopData {
    private static ShopData instance = null;

    private String shopName;
    private String address;
    private String additionalInfo;
    private Time endOrderingTime;
    private Time startCollectingTime;
    private Time endCollectingTime;

    public static synchronized ShopData getInstance() {
        if (instance == null) {
            instance = new ShopData();
        }

        return instance;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Time getEndOrderingTime() {
        return endOrderingTime;
    }

    public void setEndOrderingTime(Time endOrderingTime) {
        this.endOrderingTime = endOrderingTime;
    }

    public Time getStartCollectingTime() {
        return startCollectingTime;
    }

    public void setStartCollectingTime(Time startCollectingTime) {
        this.startCollectingTime = startCollectingTime;
    }

    public Time getEndCollectingTime() {
        return endCollectingTime;
    }

    public void setEndCollectingTime(Time endCollectingTime) {
        this.endCollectingTime = endCollectingTime;
    }
}
