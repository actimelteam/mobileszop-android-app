package splash;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Pair;

import java.util.LinkedList;

import extra.SingleLiveEvent;
import extra.Triple;
import products.Product;
import splash.api.DataLoadCallback;
import splash.api.DataLoaderService;

public class SplashViewModel extends AndroidViewModel implements DataLoadCallback {
    private DataLoaderService dataLoaderService;
    private final SingleLiveEvent<Void> successLiveData = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> failureLiveData = new SingleLiveEvent<>();
    private final MutableLiveData<Pair<LinkedList<String>, LinkedList<Triple<Product, Double, Double>>>> basketProductChangesLiveData = new MutableLiveData<>();

    public SplashViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    protected void onCleared() {
        if (dataLoaderService != null) {
            dataLoaderService.clearDisposables();
        }

        super.onCleared();
    }

    public void refreshData(Context context) {
        dataLoaderService = new DataLoaderService(context);
        dataLoaderService.startLoadingData(this);
    }

    //LiveData

    public SingleLiveEvent<Void> getSuccessLiveData() {
        return successLiveData;
    }

    public SingleLiveEvent<Void> getFailureLiveData() {
        return failureLiveData;
    }

    public MutableLiveData<Pair<LinkedList<String>, LinkedList<Triple<Product, Double, Double>>>> getBasketProductChangesLiveData() {
        return basketProductChangesLiveData;
    }

    //Eventy

    @Override
    public void dataDownloadSuccess() {
        successLiveData.call();
    }

    @Override
    public void dataDownloadFailure() {
        failureLiveData.call();
    }

    @Override
    public void dataDownloadNeedToAcceptChanges(Pair<LinkedList<String>, LinkedList<Triple<Product, Double, Double>>> productChanges) {
        basketProductChangesLiveData.postValue(productChanges);
    }
}
