package splash;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.zpi.mobileshop.R;

import java.util.LinkedList;

import extra.Triple;
import main.MainActivity;
import products.Product;

public class SplashActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private SplashViewModel splashViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        progressBar = findViewById(R.id.progressBar);

        splashViewModel = ViewModelProviders.of(this).get(SplashViewModel.class);

        splashViewModel.getSuccessLiveData().observe(this, Void -> loadNextActivity());

        splashViewModel.getFailureLiveData().observe(this, Void -> notifyFailure());

        splashViewModel.getBasketProductChangesLiveData().observe(this, productChanges -> {
            if (productChanges != null) {
                progressBar.setVisibility(View.GONE);

                LinkedList<String> unavailableProducts = productChanges.first;
                int unavailableProductsSize = unavailableProducts.size();

                LinkedList<Triple<Product, Double, Double>> priceChangedProducts = productChanges.second;
                int priceChangedProductsSize = priceChangedProducts.size();

                StringBuilder builder = new StringBuilder();

                if (unavailableProductsSize != 0) {
                    if (unavailableProductsSize == 1) {
                        builder.append(getString(R.string.splash_unavailable_product1));
                    } else {
                        builder.append(getString(R.string.splash_unavailable_products1));
                    }

                    for (String product : unavailableProducts) {
                        builder.append("- ");
                        builder.append(product);
                        builder.append('\n');
                    }

                    if (unavailableProductsSize == 1) {
                        builder.append(getString(R.string.splash_unavailable_product2));
                    } else {
                        builder.append(getString(R.string.splash_unavailable_products2));
                    }

                    if (priceChangedProductsSize != 0) {
                        builder.append("\n\n");
                    }
                }

                if (priceChangedProductsSize != 0) {
                    if (priceChangedProductsSize == 1) {
                        builder.append(getString(R.string.splash_price_changed1));
                    } else {
                        builder.append(getString(R.string.splash_price_changed2));
                    }

                    for (Triple<Product, Double, Double> tuple : priceChangedProducts) {
                        Product product = tuple.first;

                        builder.append("- ");
                        builder.append(product.getName());
                        builder.append(":\n  ");

                        product.setPrice(tuple.second);
                        builder.append(product.getPriceString(getApplicationContext()));

                        builder.append(" → ");
                        product.setPrice(tuple.third);

                        builder.append(product.getPriceString(getApplicationContext()));
                        builder.append('\n');
                    }
                }

                AlertDialog alert = new AlertDialog.Builder(SplashActivity.this).create();
                alert.setTitle(getString(R.string.splash_changes_title));
                alert.setMessage(builder.toString());
                alert.setButton(
                        AlertDialog.BUTTON_NEUTRAL,
                        getString(R.string.splash_changes_button),
                        (dialog, which) -> loadNextActivity());

                alert.show();
            }
        });

        splashViewModel.refreshData(this);
    }

    private void loadNextActivity() {
        progressBar.setVisibility(View.GONE);
        startActivity(new Intent(this, MainActivity.class));
    }

    private void notifyFailure() {
        progressBar.setVisibility(View.GONE);

        Snackbar errorInformation = Snackbar.make(findViewById(R.id.content), R.string.splash_cant_connect, Snackbar.LENGTH_INDEFINITE);
        errorInformation.setAction(R.string.splash_error_try_again, v -> tryAgainButtonPress());
        errorInformation.show();
    }

    private void tryAgainButtonPress() {
        progressBar.setVisibility(View.VISIBLE);
        splashViewModel.refreshData(this);
    }
}
