package categories.api;

import categories.Category;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class CategoryService {
    public static Category getCategoriesTree(Realm realm) {
        return realm.where(Category.class)
                .equalTo("id", 0)
                .findFirst();
    }

    public static RealmResults<Category> findCategoriesLike(Realm realm, String[] criteria) {
        RealmQuery<Category> where = realm.where(Category.class);

        for (String criterium : criteria) {
            where = where.contains("name", criterium, Case.INSENSITIVE);
        }

        return where.findAllAsync();
    }
}
