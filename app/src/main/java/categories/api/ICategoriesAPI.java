package categories.api;

import categories.Category;
import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ICategoriesAPI {
    @GET("api/categories")
    Observable<Category> getAllCategories();
}
