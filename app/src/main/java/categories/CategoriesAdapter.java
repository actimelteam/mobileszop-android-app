package categories;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;


class CategoriesAdapter extends BaseExpandableListAdapter {
    private final FragmentActivity context;
    private final Category data;
    private final int listLevel;
    private final CategoryItemView[] items;

    CategoriesAdapter(FragmentActivity context, Category data, int listLevel) {
        this.context = context;
        this.data = data;
        this.listLevel = listLevel;

        items = new CategoryItemView[data.getAmountOfSubcategories()];
    }

    @Override
    public int getGroupCount() {
        return data.getAmountOfSubcategories();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return data.getSubcategory(groupPosition).getAmountOfSubcategories();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return items[groupPosition];
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return items[groupPosition].getChild(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (items[groupPosition] == null) {
            items[groupPosition] = new CategoryItemView(data.getSubcategory(groupPosition), context, listLevel, false);
        }

        items[groupPosition].setExpanded(isExpanded);
        return items[groupPosition].getView();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        return items[groupPosition].getChild(childPosition).getView();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
