package categories;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import categories.api.CategoryService;
import io.realm.Realm;

@SuppressWarnings("WeakerAccess")
public class CategoriesViewModel extends AndroidViewModel {
    private final Realm realm;
    private MutableLiveData<Category> categoriesTree;

    public CategoriesViewModel(@NonNull Application application) {
        super(application);
        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onCleared() {
        realm.close();
        super.onCleared();
    }

    private void loadCategories() {
        categoriesTree.setValue(CategoryService.getCategoriesTree(realm));
    }

    LiveData<Category> getCategoriesTree() {
        if (categoriesTree == null) {
            categoriesTree = new MutableLiveData<>();
            loadCategories();
        }

        return categoriesTree;
    }
}
