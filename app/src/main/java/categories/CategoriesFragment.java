package categories;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.zpi.mobileshop.R;

public class CategoriesFragment extends Fragment {
    private FragmentActivity activity;
    private ExpandableListView listView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);
        CategoriesViewModel viewModel = ViewModelProviders.of(this).get(CategoriesViewModel.class);
        activity = getActivity();
        listView = view.findViewById(R.id.categoriesList);

        viewModel.getCategoriesTree().observe(this, categories -> {
            CategoriesAdapter adapter = null;

            if (categories != null) {
                adapter = new CategoriesAdapter(activity, categories, 0);
            }

            listView.setAdapter(adapter);
        });

        return view;
    }
}
