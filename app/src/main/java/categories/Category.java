package categories;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Category extends RealmObject {
    @PrimaryKey
    private long id;

    @SuppressWarnings("unused")
    private String name;

    @SuppressWarnings("CanBeFinal")
    private RealmList<Category> subcategories = new RealmList<>();

    public Category() {
    }

    private Category(long id, List<Category> subcategories) {
        this.id = id;

        if (subcategories != null) {
            this.subcategories.addAll(subcategories);
        }
    }

    public String getName() {
        return name;
    }

    int getAmountOfSubcategories() {
        return subcategories == null ? 0 : subcategories.size();
    }

    boolean hasSubcategories() {
        return subcategories != null && !subcategories.isEmpty();
    }

    Category getSubcategory(int index) {
        return subcategories.get(index);
    }

    Category exportSubcategory(int index) {
        return new Category(-1, Collections.singletonList(getSubcategory(index)));
    }

    public LinkedList<Long> getSubcategoriesRecursively() {
        LinkedList<Long> result = new LinkedList<>();
        result.addFirst(id);

        for (Category child : subcategories) {
            result.addAll(child.getSubcategoriesRecursively());
        }

        return result;
    }

    public void sortSubcategories() {
        Collections.sort(subcategories, new CategoryComparator());

        for (Category subcategory : subcategories) {
            if (subcategory != null) {
                subcategory.sortSubcategories();
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append('\n');
        result.append(name);
        result.append(String.format(Locale.getDefault(), " %d (children: %d)\n", id, getAmountOfSubcategories()));

        for (Category subtree : subcategories) {
            result.append(subtree.toString().replaceAll("\n", "\n\t"));
        }

        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return id == category.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    static class CategoryComparator implements Comparator<Category> {

        @Override
        public int compare(Category o1, Category o2) {
            String name1 = null;
            String name2 = null;

            if (o1 != null) {
                name1 = o1.name;
            }

            if (o2 != null) {
                name2 = o2.name;
            }

            return Objects.compare(name1, name2, String::compareToIgnoreCase);
        }
    }
}
