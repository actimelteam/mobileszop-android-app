package categories;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.zpi.mobileshop.R;

import java.util.Iterator;
import java.util.LinkedList;

import extra.NestedExpandableListView;
import main.MainActivity;
import products.ProductsFragment;

class CategoryItemView {

    private final Category data;
    private final CategoryItemView[] children;
    private View view;
    private final FragmentActivity context;
    private final int listLevel;

    private ImageView arrow;

    private boolean isExpanded;

    @SuppressLint("InflateParams")
    CategoryItemView(Category data, FragmentActivity context, int listLevel, boolean makeSublist) {
        this.data = data;
        this.children = new CategoryItemView[data.getAmountOfSubcategories()];
        this.context = context;
        this.listLevel = listLevel;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (inflater != null) {
            if (data.hasSubcategories()) {
                if (!makeSublist) {
                    view = inflater.inflate(R.layout.fragment_categories_group, null);

                    ImageButton imageButton = view.findViewById(R.id.btnCategoryListItems);
                    imageButton.setFocusable(false);

                    imageButton.setOnClickListener(button -> loadProductsActivity(data));
                } else {
                    view = inflater.inflate(R.layout.fragment_categories_subcategory, null);

                    NestedExpandableListView listView = view.findViewById(R.id.categoriesList);
                    CategoriesAdapter adapter = new CategoriesAdapter(context, data, listLevel + 1);

                    listView.setAdapter(adapter);
                }
            } else {
                view = inflater.inflate(R.layout.fragment_categories_item, null);
                view.setOnClickListener(view -> loadProductsActivity(data));
            }
        }

        if (!makeSublist) {
            TextView title = view.findViewById(R.id.textCategoryName);
            title.setText(data.getName());
            arrow = view.findViewById(R.id.imageCategoryArrow);

            setIndentation(view, listLevel);
        }
    }

    CategoryItemView getChild(int index) {
        if (children[index] == null) {
            if (data.getSubcategory(index).hasSubcategories()) {
                children[index] = new CategoryItemView(data.exportSubcategory(index), context, listLevel, true);
            } else {
                children[index] = new CategoryItemView(data.getSubcategory(index), context, listLevel + 1, false);
            }
        }

        return children[index];
    }

    public View getView() {
        return view;
    }

    void setExpanded(boolean expanded) {
        if (expanded != isExpanded) {
            rotateArrow();
        }

        isExpanded = expanded;
    }

    private void rotateArrow() {
        if (arrow != null) {
            AnimatedVectorDrawable drawable;

            if (!isExpanded) {
                drawable = (AnimatedVectorDrawable) context.getDrawable(R.drawable.ic_chevron_down);
            } else {
                drawable = (AnimatedVectorDrawable) context.getDrawable(R.drawable.ic_chevron_up);
            }

            if (drawable != null) {
                arrow.setImageDrawable(drawable);
                drawable.start();
            }
        }
    }

    private void setIndentation(View view, int listLevel) {
        int NEXT_LEVEL_INDENT = 100;

        View layout = view.findViewById(R.id.outerLayout);
        ConstraintLayout.MarginLayoutParams layoutParams = (ConstraintLayout.MarginLayoutParams) layout.getLayoutParams();
        layoutParams.setMarginStart(listLevel * NEXT_LEVEL_INDENT);
        layout.setLayoutParams(layoutParams);
    }

    private void loadProductsActivity(Category data) {
        LinkedList<Long> categoriesList = data.getSubcategoriesRecursively();
        long[] categoriesArray = new long[categoriesList.size()];
        Iterator<Long> iterator = categoriesList.iterator();
        int index = 0;

        Bundle bundle = new Bundle();

        while (iterator.hasNext()) {
            categoriesArray[index++] = iterator.next();
        }

        bundle.putLongArray("categories", categoriesArray);
        bundle.putString("firstCategoryName", data.getName());

        ProductsFragment productsFragment = new ProductsFragment();
        productsFragment.setArguments(bundle);

        ((MainActivity) context).loadFragment(productsFragment);
    }
}
