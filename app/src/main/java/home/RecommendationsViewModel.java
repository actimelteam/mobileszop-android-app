package home;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.zpi.mobileshop.BuildConfig;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import extra.GoogleSignInHelper;
import home.api.IRecommendationsAPI;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import products.Product;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@SuppressWarnings("WeakerAccess")
public class RecommendationsViewModel extends AndroidViewModel {
    private final MutableLiveData<List<Product>> frequentlyOrderedByUserLiveData = new MutableLiveData<>();
    private final MutableLiveData<List<Product>> frequentlyOrderedByAllLiveData = new MutableLiveData<>();
    private final MutableLiveData<Product> openProductDetailsLiveData = new MutableLiveData<>();

    private final Realm realm;
    private final CompositeDisposable disposable = new CompositeDisposable();

    public RecommendationsViewModel(@NonNull Application application) {
        super(application);
        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onCleared() {
        realm.close();
        disposable.clear();
        super.onCleared();
    }

    public void loadRecommendations() {
        IRecommendationsAPI recommendationsAPI = new Retrofit.Builder()
                .baseUrl(BuildConfig.WebAPIAddress)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(IRecommendationsAPI.class);

        GoogleSignInHelper helper = GoogleSignInHelper.getInstance();

        if (helper.isSignedIn()) {
            disposable.add(
                    recommendationsAPI
                            .getTopPopularProductsForClient(
                                    helper.getCurrentAccount().getIdToken(),
                                    Integer.parseInt(BuildConfig.RecommendationsAmount)
                            )
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    this::postFrequentlyOrderedByUser,
                                    throwable -> {
                                        Log.e("CategoryService", "Error: Can't download user recommendations from server!\n" + throwable.getMessage());
                                        throwable.printStackTrace();
                                    }
                            )
            );
        }

        disposable.add(
                recommendationsAPI
                        .getTopPopularProducts(
                                Integer.parseInt(BuildConfig.RecommendationsAmount)
                        )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                this::postFrequentlyOrderedByAll,
                                throwable -> {
                                    Log.e("CategoryService", "Error: Can't download categories from server!\n" + throwable.getMessage());
                                    throwable.printStackTrace();
                                }
                        )
        );
    }

    private void postFrequentlyOrderedByUser(@NonNull List<Long> productIds) {
        List<Product> recommendedForYou = new LinkedList<>();
        final AtomicInteger amountLoaded = new AtomicInteger();
        final int size = productIds.size();

        for (Long productId : productIds) {
            Product productAsync = realm.where(Product.class).equalTo("id", productId).findFirstAsync();
            recommendedForYou.add(productAsync);

            productAsync.addChangeListener(new RealmChangeListener<Product>() {
                @Override
                public void onChange(@NonNull Product product) {
                    product.removeChangeListener(this);

                    if (amountLoaded.incrementAndGet() == size) {
                        frequentlyOrderedByUserLiveData.postValue(recommendedForYou);
                    }
                }
            });
        }
    }

    private void postFrequentlyOrderedByAll(@NonNull List<Long> productIds) {
        List<Product> mostFrequentlyOrdered = new LinkedList<>();
        final AtomicInteger amountLoaded = new AtomicInteger();
        final int size = productIds.size();

        for (Long productId : productIds) {
            Product productAsync = realm.where(Product.class).equalTo("id", productId).findFirstAsync();
            mostFrequentlyOrdered.add(productAsync);

            productAsync.addChangeListener(new RealmChangeListener<Product>() {
                @Override
                public void onChange(@NonNull Product product) {
                    product.removeChangeListener(this);

                    if (amountLoaded.incrementAndGet() == size) {
                        frequentlyOrderedByAllLiveData.postValue(mostFrequentlyOrdered);
                    }
                }
            });
        }
    }

    public void openProduct(Product product) {
        openProductDetailsLiveData.postValue(product);
    }

    public MutableLiveData<List<Product>> getFrequentlyOrderedByUserLiveData() {
        return frequentlyOrderedByUserLiveData;
    }

    public MutableLiveData<List<Product>> getFrequentlyOrderedByAllLiveData() {
        return frequentlyOrderedByAllLiveData;
    }

    public MutableLiveData<Product> getOpenProductDetailsLiveData() {
        return openProductDetailsLiveData;
    }
}
