package home.api;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IRecommendationsAPI {
    @GET("api/products/popular/{topAmount}")
    Observable<List<Long>> getTopPopularProducts(
            @Path("topAmount") int topAmount
    );

    @GET("api/products/popular/{userToken}/{topAmount}")
    Observable<List<Long>> getTopPopularProductsForClient(
            @Path("userToken") String userToken,
            @Path("topAmount") int topAmount
    );
}
