package home;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.zpi.mobileshop.R;

import main.basket.BasketViewModel;
import products.ProductDialogFragment;

public class HomeFragment extends Fragment {
    private SwipeRefreshLayout homeSwipeRefreshLayout;

    private CardView orderedByUserLayout;
    private CardView orderedByAllLayout;

    private RecyclerView orderedByUserList;
    private RecyclerView orderedByAllList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecommendationsViewModel recommendationsViewModel = ViewModelProviders.of(this).get(RecommendationsViewModel.class);
        homeSwipeRefreshLayout = view.findViewById(R.id.homeSwipeRefreshLayout);
        orderedByUserLayout = view.findViewById(R.id.orderedByUserLayout);
        orderedByAllLayout = view.findViewById(R.id.orderedByAllLayout);

        orderedByUserList = view.findViewById(R.id.orderedByUserList);
        orderedByUserList.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
        orderedByUserList.setAdapter(new RecommendationsAdapter(recommendationsViewModel));

        orderedByAllList = view.findViewById(R.id.orderedByAllList);
        orderedByAllList.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
        orderedByAllList.setAdapter(new RecommendationsAdapter(recommendationsViewModel));

        recommendationsViewModel.getFrequentlyOrderedByUserLiveData().observe(this, products -> {
            RecommendationsAdapter adapter = (RecommendationsAdapter) orderedByUserList.getAdapter();
            adapter.setCurrentData(products);
            homeSwipeRefreshLayout.setRefreshing(false);
            showFrequentlyOrderedByUser();
        });

        recommendationsViewModel.getFrequentlyOrderedByAllLiveData().observe(this, products -> {
            RecommendationsAdapter adapter = (RecommendationsAdapter) orderedByAllList.getAdapter();
            adapter.setCurrentData(products);
            homeSwipeRefreshLayout.setRefreshing(false);
            showFrequentlyOrderedByAll();
        });

        recommendationsViewModel.getOpenProductDetailsLiveData().observe(this, product -> {
            FragmentActivity activity = getActivity();

            if (product != null && activity != null) {
                ProductDialogFragment dialog = new ProductDialogFragment();
                BasketViewModel basketViewModel = ViewModelProviders.of(activity).get(BasketViewModel.class);

                dialog.setParameters(product, basketViewModel);
                dialog.show(getActivity().getSupportFragmentManager(), "PRODUCT");
            }
        });

        homeSwipeRefreshLayout.setRefreshing(true);
        recommendationsViewModel.loadRecommendations();

        homeSwipeRefreshLayout.setOnRefreshListener(recommendationsViewModel::loadRecommendations);
    }

    private void showFrequentlyOrderedByUser() {
        orderedByUserLayout.setVisibility(View.VISIBLE);
        orderedByUserLayout.setAnimation(getFadeInAnimation());
    }

    private void showFrequentlyOrderedByAll() {
        orderedByAllLayout.setVisibility(View.VISIBLE);
        orderedByAllLayout.setAnimation(getFadeInAnimation());
    }

    private Animation getFadeInAnimation() {
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new AccelerateInterpolator());
        fadeIn.setDuration(500);
        return fadeIn;
    }
}
