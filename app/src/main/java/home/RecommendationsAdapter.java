package home;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zpi.mobileshop.R;
import com.zpi.mobileshop.databinding.FragmentHomeGridElementBinding;

import java.util.LinkedList;
import java.util.List;

import products.Product;
import products.ProductsListDiffCallback;

class RecommendationsAdapter extends RecyclerView.Adapter<RecommendationViewHolder> {
    private List<Product> currentData = new LinkedList<>();
    private final RecommendationsViewModel recommendationsViewModel;

    RecommendationsAdapter(RecommendationsViewModel recommendationsViewModel) {
        this.recommendationsViewModel = recommendationsViewModel;
    }

    @NonNull
    @Override
    public RecommendationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        FragmentHomeGridElementBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_home_grid_element, parent, false);
        return new RecommendationViewHolder(binding, recommendationsViewModel);
    }

    @Override
    public void onBindViewHolder(@NonNull RecommendationViewHolder holder, int position) {
        Product product = currentData.get(position);

        if (product != null) {
            holder.bind(product);
        }
    }

    @Override
    public int getItemCount() {
        return currentData.size();
    }

    public void setCurrentData(@Nullable List<Product> newData) {
        if (newData == null) {
            newData = new LinkedList<>();
        }

        DiffUtil
                .calculateDiff(new ProductsListDiffCallback(this.currentData, newData))
                .dispatchUpdatesTo(this);

        this.currentData = newData;
    }
}
