package home;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.zpi.mobileshop.databinding.FragmentHomeGridElementBinding;

import products.Product;

public class RecommendationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final FragmentHomeGridElementBinding binding;
    private final RecommendationsViewModel recommendationsViewModel;

    RecommendationViewHolder(FragmentHomeGridElementBinding binding, RecommendationsViewModel recommendationsViewModel) {
        super(binding.getRoot());
        this.binding = binding;
        this.recommendationsViewModel = recommendationsViewModel;
    }

    void bind(@NonNull Product product) {
        binding.getRoot().setOnClickListener(this);
        binding.setProduct(product);
        binding.executePendingBindings();
    }

    @Override
    public void onClick(View v) {
        recommendationsViewModel.openProduct(binding.getProduct());
    }
}
