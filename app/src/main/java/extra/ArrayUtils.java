package extra;

import java.util.Iterator;
import java.util.List;

public class ArrayUtils {
    public static Long[] convertPrimitiveToObjectArray(long[] array) {
        int length = array.length;
        Long[] result = new Long[length];

        for (int i = 0; i < length; i++) {
            result[i] = array[i];
        }

        return result;
    }

    public static long[] convertListToPrimitiveArray(List<Long> list) {
        int length = list.size();
        long[] result = new long[length];
        Iterator<Long> iterator = list.iterator();
        int i = 0;

        while (iterator.hasNext()) {
            result[i++] = iterator.next();
        }

        return result;
    }

    public static Long[] convertListToObjectArray(List<Long> list) {
        int length = list.size();
        Long[] result = new Long[length];
        Iterator<Long> iterator = list.iterator();
        int i = 0;

        while (iterator.hasNext()) {
            result[i++] = iterator.next();
        }

        return result;
    }
}
