package extra;

import java.util.Calendar;
import java.util.Locale;
import java.util.Scanner;

public class Time {
    public int hour;
    public int minute;

    @SuppressWarnings("WeakerAccess")
    public Time(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public static Time parseTime(String time) {
        Scanner scanner = new Scanner(time);
        scanner.useDelimiter(":");
        return new Time(scanner.nextShort(), scanner.nextShort());
    }

    public static Time now() {
        Calendar calendar = Calendar.getInstance();

        return new Time(
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE)
        );
    }

    @SuppressWarnings("WeakerAccess")
    public int getTimestamp() {
        return hour * 60 + minute;
    }

    public boolean isGreaterThan(Time other) {
        return getTimestamp() > other.getTimestamp();
    }

    public boolean isInRange(Time rangeFrom, Time rangeTo) {
        int timestamp = getTimestamp();
        return timestamp >= rangeFrom.getTimestamp() && timestamp <= rangeTo.getTimestamp();
    }

    public String toString() {
        return String.format(Locale.getDefault(), "%02d:%02d", hour, minute);
    }
}
