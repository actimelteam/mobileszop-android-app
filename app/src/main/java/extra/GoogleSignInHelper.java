package extra;

import android.app.Application;
import android.content.Intent;
import android.util.Pair;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.zpi.mobileshop.BuildConfig;

public class GoogleSignInHelper {
    private static GoogleSignInHelper instance = null;

    private Application application;
    private GoogleSignInClient client;

    private static final int ACTIVITY_RESULT_SIGN_IN = 50;

    public enum StatusCode {
        STATUS_OK,
        STATUS_CANCELLED,
        STATUS_FAILURE
    }

    private GoogleSignInHelper() {
    }

    public static synchronized GoogleSignInHelper getInstance() {
        if (instance == null) {
            instance = new GoogleSignInHelper();
        }

        return (instance);
    }

    public void prepare(Application application) {
        this.application = application;

        GoogleSignInOptions signOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(BuildConfig.GoogleSignInKey)
                .requestEmail()
                .build();

        client = GoogleSignIn.getClient(application, signOptions);

        client.silentSignIn();
    }

    public boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(application) != null;
    }

    public GoogleSignInAccount getCurrentAccount() {
        return GoogleSignIn.getLastSignedInAccount(application);
    }

    public Pair<Intent, Integer> startSignIn() {
        Intent signInIntent = client.getSignInIntent();
        return new Pair<>(signInIntent, ACTIVITY_RESULT_SIGN_IN);
    }

    public GoogleSignInStatus handleSignInResult(int requestCode, Intent data) {
        if (requestCode == ACTIVITY_RESULT_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                return new GoogleSignInStatus(StatusCode.STATUS_OK, account, 0);
            } catch (ApiException e) {
                if (e.getStatusCode() == GoogleSignInStatusCodes.SIGN_IN_FAILED) {
                    return new GoogleSignInStatus(StatusCode.STATUS_FAILURE, null, e.getStatusCode());
                }
            }

            return new GoogleSignInStatus(StatusCode.STATUS_CANCELLED, null, null);
        }

        return null;
    }

    public void startSignOut(OnCompleteListener<Void> listener) {
        client.signOut().addOnCompleteListener(listener);
    }

    public class GoogleSignInStatus {
        public final StatusCode status;
        public final GoogleSignInAccount account;
        public final Integer errorCode;

        GoogleSignInStatus(StatusCode status, GoogleSignInAccount account, Integer errorCode) {
            this.status = status;
            this.account = account;
            this.errorCode = errorCode;
        }
    }
}
