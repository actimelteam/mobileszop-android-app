package extra;

import android.arch.lifecycle.LiveData;

import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;
import io.realm.RealmResults;

public class RealmLiveData<T extends RealmModel> extends LiveData<RealmResults<T>> {

    private final RealmChangeListener<RealmResults<T>> listener = this::setValue;
    private final RealmResults<T> results;

    public RealmLiveData(RealmResults<T> realmResults) {
        results = realmResults;
    }

    public void addChangeListener(OrderedRealmCollectionChangeListener<RealmResults<T>> listener) {
        results.addChangeListener(listener);
    }

    public RealmResults<T> getResults() {
        return results;
    }

    @Override
    protected void onActive() {
        results.addChangeListener(listener);
    }

    @Override
    protected void onInactive() {
        results.removeChangeListener(listener);
    }
}
