package extra;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmModel;
import io.realm.RealmResults;


public abstract class RealmAdapter<T extends RealmModel, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    private final OrderedRealmCollectionChangeListener<RealmResults<T>> listener;

    private RealmResults<T> currentData;

    protected RealmAdapter() {
        listener = (data1, changeSet) -> {
            OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
            for (int i = deletions.length - 1; i >= 0; i--) {
                OrderedCollectionChangeSet.Range range = deletions[i];
                notifyItemRangeRemoved(range.startIndex, range.length);
            }

            OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
            for (OrderedCollectionChangeSet.Range range : insertions) {
                notifyItemRangeInserted(range.startIndex, range.length);
            }

            OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
            for (OrderedCollectionChangeSet.Range range : modifications) {
                notifyItemRangeChanged(range.startIndex, range.length);
            }
        };
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        if (currentData != null) {
            currentData.addChangeListener(listener);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        if (currentData != null) {
            currentData.removeChangeListener(listener);
        }

        super.onDetachedFromRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return currentData != null && currentData.isValid() ? currentData.size() : 0;
    }

    protected T getItem(int index) {
        return currentData != null && currentData.isValid() ? currentData.get(index) : null;
    }

    public void setCurrentData(@Nullable RealmResults<T> newData) {
        if (currentData != null) {
            currentData.removeChangeListener(listener);
        }

        calculateDiff(currentData, newData).dispatchUpdatesTo(this);
        currentData = newData;

        if (currentData != null) {
            currentData.addChangeListener(listener);
        }
    }

    protected abstract DiffUtil.DiffResult calculateDiff(RealmResults<T> oldData, RealmResults<T> newData);
}
