package extra;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class KeyboardUtil {
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imeManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imeManager != null) {
            View view = activity.getCurrentFocus();

            if (view == null) {
                view = new View(activity);
            }

            imeManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
