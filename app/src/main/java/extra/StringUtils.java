package extra;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;

public class StringUtils {
    public static String capitalizeFirstLetter(String input) {
        char firstChar = input.charAt(0);

        if (firstChar >= 'a' && firstChar <= 'z') {
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        } else {
            return input;
        }
    }

    public static Spannable makeWordsInStringBold(String string, String[] words) {
        Spannable spannable = new SpannableString(string);
        string = string.toLowerCase();

        for (String word : words) {
            word = word.toLowerCase();
            int startIndex = string.indexOf(word);
            int criteriumLength = word.length();

            while (startIndex >= 0) {
                spannable.setSpan(new StyleSpan(Typeface.BOLD), startIndex, startIndex + criteriumLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                startIndex = string.indexOf(word, startIndex + criteriumLength);
            }
        }

        return spannable;
    }
}
