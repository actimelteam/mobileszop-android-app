package extra;

import android.util.Pair;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GsonKeyValueConverter implements JsonDeserializer<List<Pair<String, String>>> {

    @Override
    public List<Pair<String, String>> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        LinkedList<Pair<String, String>> result = new LinkedList<>();
        JsonObject obj = json.getAsJsonObject();
        Set<Map.Entry<String, JsonElement>> entries = obj.entrySet();

        for (Map.Entry<String, JsonElement> pair : entries) {
            result.add(new Pair<>(pair.getKey(), pair.getValue().getAsString()));
        }

        return result;
    }
}
