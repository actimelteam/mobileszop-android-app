package settings;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.View;

import com.zpi.mobileshop.R;

import java.util.Objects;

import splash.SplashActivity;

public class SettingsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceClickListener {
    private SettingsViewModel settingsViewModel;

    private Preference option1;
    private Preference option2;
    private Preference refreshDataOption;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        settingsViewModel = ViewModelProviders.of(this).get(SettingsViewModel.class);

        option1 = findPreference("user_name");
        option2 = findPreference("sign_in_out");
        option2.setOnPreferenceClickListener(this);

        refreshDataOption = findPreference("refreshData");
        refreshDataOption.setOnPreferenceClickListener(this);

        settingsViewModel.getAccountLiveData().observe(this, account -> {
            if (account == null) {
                option1.setIcon(R.drawable.ic_account_circle);
                option1.setTitle(getString(R.string.pref_not_signed_in));
                option1.setSummary(getString(R.string.pref_sign_in_and_start_shopping));

                option2.setTitle(getString(R.string.pref_sign_in));
            } else {
                option1.setTitle(account.getDisplayName());
                option1.setSummary(account.getEmail());

                option2.setTitle(R.string.pref_sign_out);
            }
        });

        settingsViewModel.getAvatarLiveData().observe(this, avatar -> {
            if (avatar != null) {
                option1.setIcon(avatar);
            }
        });

        settingsViewModel.getStartIntentEvent().observe(this, intentAndResultCode -> {
            if (intentAndResultCode != null) {
                startActivityForResult(intentAndResultCode.first, intentAndResultCode.second);
            }
        });

        settingsViewModel.getFailureEvent().observe(this, errorCode -> {
            View root = getView();
            Activity activity = getActivity();

            if (root != null && activity != null) {
                Snackbar.make(root, String.format(activity.getString(R.string.onboarding_toast_error), errorCode), Snackbar.LENGTH_LONG);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        settingsViewModel.getStartIntentEvent().removeObservers(this);
        settingsViewModel.getFailureEvent().removeObservers(this);
        settingsViewModel.getAccountLiveData().removeObservers(this);
        settingsViewModel.getAvatarLiveData().removeObservers(this);
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (preference.equals(option2)) {
            settingsViewModel.signInOutButtonPress();
            return true;
        }

        if (preference.equals(refreshDataOption)) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            sharedPreferences.edit().remove("LastDataRefreshDate").apply();

            startActivity(new Intent(getContext(), SplashActivity.class));
            Objects.requireNonNull(getActivity()).finish();
            return true;
        }

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        settingsViewModel.handleActivityResult(requestCode, data);
    }
}