package settings;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.io.InputStream;
import java.net.URL;

import extra.GoogleSignInHelper;
import extra.SingleLiveEvent;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

@SuppressWarnings("WeakerAccess")
public class SettingsViewModel extends AndroidViewModel {
    private final GoogleSignInHelper signInHelper;
    private final SingleLiveEvent<Pair<Intent, Integer>> startIntentEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Integer> failureEvent = new SingleLiveEvent<>();
    private final MutableLiveData<GoogleSignInAccount> accountLiveData = new MutableLiveData<>();
    private final MutableLiveData<RoundedBitmapDrawable> avatarLiveData = new MutableLiveData<>();

    private final CompositeDisposable disposable = new CompositeDisposable();

    public SettingsViewModel(@NonNull Application application) {
        super(application);
        signInHelper = GoogleSignInHelper.getInstance();

        if (signInHelper.isSignedIn()) {
            refreshUI(signInHelper.getCurrentAccount());
        } else {
            accountLiveData.postValue(null);
        }
    }

    @Override
    protected void onCleared() {
        disposable.clear();
        super.onCleared();
    }

    void signInOutButtonPress() {
        if (signInHelper.isSignedIn()) {
            signInHelper.startSignOut(task -> accountLiveData.postValue(null));
        } else {
            Pair<Intent, Integer> intentAndResultCode = signInHelper.startSignIn();
            startIntentEvent.postValue(intentAndResultCode);
        }
    }

    void handleActivityResult(int requestCode, Intent data) {
        GoogleSignInHelper.GoogleSignInStatus result = signInHelper.handleSignInResult(requestCode, data);

        if (result != null) {
            if (result.status == GoogleSignInHelper.StatusCode.STATUS_OK) {
                refreshUI(signInHelper.getCurrentAccount());
            } else if (result.status == GoogleSignInHelper.StatusCode.STATUS_FAILURE) {
                failureEvent.postValue(result.errorCode);
            }
        }
    }

    private void refreshUI(GoogleSignInAccount account) {
        Uri uri = account.getPhotoUrl();

        accountLiveData.postValue(account);

        if (uri == null) {
            avatarLiveData.postValue(null);
        } else {
            String url = String.valueOf(account.getPhotoUrl());

            Observable<RoundedBitmapDrawable> observable = Observable.fromCallable(() -> {
                InputStream stream = new URL(url).openStream();
                Resources resources = getApplication().getResources();
                int side = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 36, resources.getDisplayMetrics()));

                RoundedBitmapDrawable image = RoundedBitmapDrawableFactory.create(
                        resources,
                        stream
                );

                image.setCircular(true);
                image.setBounds(new Rect(0, 0, side, side));
                return image;
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

            disposable.add(
                    observable.subscribe(
                            avatarLiveData::postValue,

                            throwable -> {
                                Log.e("PreferencesFragment", "Can't download avatar photo!");
                                throwable.printStackTrace();
                            })
            );
        }
    }

    SingleLiveEvent<Pair<Intent, Integer>> getStartIntentEvent() {
        return startIntentEvent;
    }

    SingleLiveEvent<Integer> getFailureEvent() {
        return failureEvent;
    }

    MutableLiveData<GoogleSignInAccount> getAccountLiveData() {
        return accountLiveData;
    }

    MutableLiveData<RoundedBitmapDrawable> getAvatarLiveData() {
        return avatarLiveData;
    }
}
