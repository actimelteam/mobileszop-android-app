package products;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.zpi.mobileshop.databinding.FragmentProductsItemBinding;

import main.basket.BasketViewModel;

public class ProductViewHolder extends RecyclerView.ViewHolder {
    private final FragmentProductsItemBinding binding;

    ProductViewHolder(FragmentProductsItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    void bind(@NonNull Product product, BasketViewModel basketViewModel) {
        binding.setProduct(product);
        binding.setViewModel(basketViewModel);
        binding.executePendingBindings();
    }

    void setOnClickListener(View.OnClickListener listener) {
        binding.getRoot().setOnClickListener(listener);
    }
}