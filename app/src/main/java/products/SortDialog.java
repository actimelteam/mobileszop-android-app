package products;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.zpi.mobileshop.R;

import java.util.Objects;

public class SortDialog extends DialogFragment {
    interface SortDialogListener {
        void onSelectSortOption(ProductsFragment.SortingOption sortingOption);
    }

    private SortDialogListener sortDialogListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));

        String[] items = {
                getString(R.string.products_sort_name_asc),
                getString(R.string.products_sort_name_desc),
                getString(R.string.products_sort_price_asc),
                getString(R.string.products_sort_price_desc)
        };

        sortDialogListener = (SortDialogListener) getTargetFragment();

        builder.setTitle(R.string.products_sort_by)
                .setItems(items, (dialogInterface, which) -> {
                    switch (which) {
                        case 0:
                            sortDialogListener.onSelectSortOption(ProductsFragment.SortingOption.NAME_ASC);
                            return;

                        case 1:
                            sortDialogListener.onSelectSortOption(ProductsFragment.SortingOption.NAME_DESC);
                            return;

                        case 2:
                            sortDialogListener.onSelectSortOption(ProductsFragment.SortingOption.PRICE_ASC);
                            return;

                        case 3:
                            sortDialogListener.onSelectSortOption(ProductsFragment.SortingOption.PRICE_DESC);
                    }
                });

        return builder.create();
    }
}
