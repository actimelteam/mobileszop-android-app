package products;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zpi.mobileshop.R;

import java.security.InvalidParameterException;

import extra.ArrayUtils;
import main.basket.BasketViewModel;


public class ProductsFragment extends Fragment implements SortDialog.SortDialogListener {
    enum SortingOption {
        NAME_ASC,
        NAME_DESC,
        PRICE_ASC,
        PRICE_DESC
    }

    private ProductsViewModel productsViewModel;
    private ProductsAdapter adapter;

    private Long[] categories;
    private String firstCategoryName;
    private long productId;

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);

        if (args != null) {
            long[] categories2 = args.getLongArray("categories");

            if (categories2 != null) {
                categories = ArrayUtils.convertPrimitiveToObjectArray(categories2);
            }

            firstCategoryName = args.getString("firstCategoryName");
            productId = args.getLong("showProductId", -1);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        FragmentActivity activity = getActivity();
        assert activity != null;

        productsViewModel = ViewModelProviders.of(this).get(ProductsViewModel.class);
        BasketViewModel basketViewModel = ViewModelProviders.of(activity).get(BasketViewModel.class);

        if (categories != null) {
            productsViewModel.setProductCategories(categories);
        } else if (productId != -1) {
            productsViewModel.setProductId(productId);
        } else {
            throw new InvalidParameterException("ProductsFragment: No information what products to show.");
        }

        RecyclerView list = view.findViewById(R.id.productsList);
        adapter = new ProductsAdapter(activity, basketViewModel);

        DefaultItemAnimator animator = new DefaultItemAnimator();
        animator.setSupportsChangeAnimations(false);

        list.setLayoutManager(new LinearLayoutManager(activity));
        list.addItemDecoration(new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL));
        list.setItemAnimator(animator);
        list.setAdapter(adapter);

        productsViewModel.getProductsLiveData().observe(this, products ->
                adapter.setCurrentData(products)
        );

        productsViewModel.updateProductsLiveData();
        return view;
    }

    public void openSortDialog() {
        Activity activity = getActivity();

        if (activity != null) {
            SortDialog dialog = new SortDialog();
            dialog.setTargetFragment(this, 0);
            dialog.show(getActivity().getSupportFragmentManager(), "Sort");
        }
    }

    @Override
    public void onSelectSortOption(ProductsFragment.SortingOption sortingOption) {
        productsViewModel.setSortingOption(sortingOption);
        productsViewModel.updateProductsLiveData();
    }

    public void setSearchCriteria(String[] criteria) {
        productsViewModel.setSearchCriteria(criteria);
        productsViewModel.updateProductsLiveData();
    }

    public String getFirstCategoryName() {
        return firstCategoryName;
    }
}
