package products;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.zpi.mobileshop.R;

import java.io.IOException;
import java.util.Locale;
import java.util.Objects;

import categories.Category;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.Ignore;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;
import main.basket.BasketItem;

public class Product extends RealmObject {
    @LinkingObjects("product")
    private final RealmResults<BasketItem> basketItem = null;
    @PrimaryKey
    private long id;
    private String name;
    private double price;
    private String description;
    private int unit;
    private int status;
    private String photoUrl;
    private long categoryId;
    @Ignore
    private Category category;

    public Product() {
    }

    public Product(long id, Category category, String name, double price, Unit unit) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.price = price;
        setUnit(unit);
    }

    public Product(Product other) {
        this.id = other.id;
        this.name = other.name;
        this.price = other.price;
        this.description = other.description;
        this.unit = other.unit;
        this.status = other.status;
        this.photoUrl = other.photoUrl;
        this.categoryId = other.categoryId;
    }

    @BindingAdapter({"android:src"})
    public static void loadImage(ImageView view, String photoUrl) {
        Glide
                .with(view.getContext())
                .load(photoUrl)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_product_placeholder)
                        .error(R.drawable.ic_product_placeholder))
                .into(view);
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPriceString(Context context) {
        switch (Unit.values()[unit]) {
            case KILOGRAM:
                return String.format(Locale.getDefault(), context.getString(R.string.price_per_kg), price);

            case GRAM:
                return String.format(Locale.getDefault(), context.getString(R.string.price_per_g), price);

            case PIECE:
                return String.format(Locale.getDefault(), context.getString(R.string.price_per_piece), price);

            case LITER:
                return String.format(Locale.getDefault(), context.getString(R.string.price_per_litre), price);
        }

        return String.format(Locale.getDefault(), context.getString(R.string.price_universal), price);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean hasDescription() {
        return description != null && description.length() != 0;
    }

    public BasketItem getBasketItem() {
        //noinspection ConstantConditions
        if (basketItem != null && basketItem.size() != 0) {
            return basketItem.first();
        } else {
            return null;
        }
    }

    public Unit getUnit() {
        return Unit.values()[unit];
    }

    private void setUnit(Unit unit) {
        Unit[] values = Unit.values();

        for (int i = 0; i < values.length; i++) {
            if (unit == values[i]) {
                this.unit = i;
                break;
            }
        }
    }

    private void setUnit(String unit) {
        setUnit(Unit.valueOf(unit));
    }

    public Status getStatus() {
        return Status.values()[status];
    }

    public String getStatusString(Context context) {
        switch (status) {
            case 0: {
                return context.getString(R.string.product_status_available);
            }

            case 1: {
                return context.getString(R.string.product_status_unavailable);
            }

            default: {
                return context.getString(R.string.product_status_unknown);
            }
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void setStatus(Status status) {
        Status[] values = Status.values();

        for (int i = 0; i < values.length; i++) {
            if (status == values[i]) {
                this.status = i;
                break;
            }
        }
    }

    private void setStatus(String status) {
        setStatus(Status.valueOf(status));
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    private void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    private void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getAmountString(Context context) {
        float amount = 0F;
        BasketItem item = getBasketItem();

        if (item != null) {
            amount = item.getAmount();
        }

        switch (getUnit()) {
            case KILOGRAM:
                return String.format(Locale.getDefault(), context.getString(R.string.amount_kg), amount);

            case GRAM:
                return String.format(Locale.getDefault(), context.getString(R.string.amount_g), amount);

            case PIECE:
                return String.format(Locale.getDefault(), context.getString(R.string.amount_pcs), amount);

            case LITER:
                return String.format(Locale.getDefault(), context.getString(R.string.amount_ltr), amount);

            default:
                return String.format(Locale.getDefault(), "%f", amount);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return id == product.id &&
                Double.compare(product.price, price) == 0 &&
                unit == product.unit &&
                status == product.status &&
                categoryId == product.categoryId &&
                Objects.equals(name, product.name) &&
                Objects.equals(description, product.description) &&
                Objects.equals(photoUrl, product.photoUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(basketItem, id, name, price, description, unit, status, photoUrl, categoryId, category);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", unit=" + unit +
                ", status=" + status +
                ", photoUrl='" + photoUrl + '\'' +
                ", categoryId=" + categoryId +
                ", category=" + category +
                '}';
    }

    public enum Unit {
        KILOGRAM, GRAM, PIECE, LITER
    }

    public enum Status {
        AVAILABLE, UNAVAILABLE
    }

    public static class ProductTypeAdapter extends TypeAdapter<Product> {

        @Override
        public void write(JsonWriter out, Product value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Product read(JsonReader in) throws IOException {
            Product product = new Product();
            in.beginObject();

            while (in.hasNext()) {
                switch (in.nextName()) {
                    case "id": {
                        product.setId(in.nextLong());
                        break;
                    }

                    case "name": {
                        product.setName(in.nextString());
                        break;
                    }

                    case "price": {
                        product.setPrice(in.nextDouble());
                        break;
                    }

                    case "description": {
                        product.setDescription(in.nextString());
                        break;
                    }

                    case "unit": {
                        product.setUnit(in.nextString());
                        break;
                    }

                    case "status": {
                        product.setStatus(in.nextString());
                        break;
                    }

                    case "categoryId": {
                        long categoryID = in.nextLong();
                        product.setCategoryId(categoryID);
                        break;
                    }

                    case "photoUrl": {
                        product.setPhotoUrl(in.nextString());
                        break;
                    }

                    case "lastUpdateDate": {
                        in.nextString();
                    }
                }
            }

            in.endObject();
            return product;
        }
    }

}