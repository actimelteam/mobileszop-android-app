package products;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.util.DiffUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zpi.mobileshop.R;
import com.zpi.mobileshop.databinding.FragmentProductsItemBinding;

import extra.RealmAdapter;
import io.realm.RealmResults;
import main.basket.BasketViewModel;

class ProductsAdapter extends RealmAdapter<Product, ProductViewHolder> {
    private final FragmentActivity activity;
    private final BasketViewModel basketViewModel;

    ProductsAdapter(FragmentActivity activity, BasketViewModel basketViewModel) {
        this.activity = activity;
        this.basketViewModel = basketViewModel;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        FragmentProductsItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_products_item, parent, false);
        return new ProductViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = getItem(position);

        if (product != null) {
            holder.bind(product, basketViewModel);
        }

        holder.setOnClickListener(view -> {
            ProductDialogFragment dialog = new ProductDialogFragment();
            dialog.setParameters(product, basketViewModel);
            dialog.show(activity.getSupportFragmentManager(), "PRODUCT");
        });
    }

    @Override
    protected DiffUtil.DiffResult calculateDiff(RealmResults<Product> oldData, RealmResults<Product> newData) {
        return DiffUtil.calculateDiff(new ProductsRealmResultDiffCallback(oldData, newData));
    }
}
