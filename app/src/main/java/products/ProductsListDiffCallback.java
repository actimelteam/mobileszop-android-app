package products;

import android.support.v7.util.DiffUtil;

import java.util.List;
import java.util.Objects;

public class ProductsListDiffCallback extends DiffUtil.Callback {
    private final List<Product> oldData;
    private final List<Product> newData;

    public ProductsListDiffCallback(List<Product> oldData, List<Product> newData) {
        this.oldData = oldData;
        this.newData = newData;
    }

    @Override
    public int getOldListSize() {
        return oldData.size();
    }

    @Override
    public int getNewListSize() {
        return newData.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Product oldP = oldData.get(oldItemPosition);
        Product newP = newData.get(newItemPosition);

        if (oldP == null) {
            return newP == null;
        } else {
            return newP != null && oldP.getId() == newP.getId();
        }
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return Objects.equals(oldData.get(oldItemPosition), newData.get(newItemPosition));
    }
}
