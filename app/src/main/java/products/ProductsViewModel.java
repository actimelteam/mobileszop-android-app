package products;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

@SuppressWarnings("WeakerAccess")
public class ProductsViewModel extends AndroidViewModel {
    private final Realm realm;
    private final MutableLiveData<RealmResults<Product>> productsLiveData = new MutableLiveData<>();

    private Long[] categories;
    private String[] searchCriteria;
    private ProductsFragment.SortingOption sortingOption;

    private long productId;

    public ProductsViewModel(@NonNull Application application) {
        super(application);
        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onCleared() {
        realm.close();
        super.onCleared();
    }

    public void setProductCategories(Long[] categories) {
        this.categories = categories;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public void setSearchCriteria(String[] searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    public void setSortingOption(ProductsFragment.SortingOption sortingOption) {
        this.sortingOption = sortingOption;
    }

    public void updateProductsLiveData() {
        RealmQuery<Product> query = realm.where(Product.class);

        if (categories != null) {
            query.in("categoryId", categories);

            if (searchCriteria != null) {
                for (String criterium : searchCriteria) {
                    query.contains("name", criterium, Case.INSENSITIVE);
                }
            }

            if (sortingOption != null) {
                switch (sortingOption) {
                    case NAME_ASC: {
                        query.sort("name", Sort.ASCENDING);
                        break;
                    }

                    case NAME_DESC: {
                        query.sort("name", Sort.DESCENDING);
                        break;
                    }

                    case PRICE_ASC: {
                        query.sort("price", Sort.ASCENDING);
                        break;
                    }

                    case PRICE_DESC: {
                        query.sort("price", Sort.DESCENDING);
                        break;
                    }
                }
            }
        } else {
            query.equalTo("id", productId);
        }

        query.findAllAsync().addChangeListener(new RealmChangeListener<RealmResults<Product>>() {
            @Override
            public void onChange(@NonNull RealmResults<Product> products) {
                productsLiveData.postValue(products);
                products.removeChangeListener(this);
            }
        });
    }

    public MutableLiveData<RealmResults<Product>> getProductsLiveData() {
        return productsLiveData;
    }
}