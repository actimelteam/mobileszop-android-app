package products;

import android.support.v7.util.DiffUtil;

import java.util.Objects;

import io.realm.RealmResults;

class ProductsRealmResultDiffCallback extends DiffUtil.Callback {
    private final RealmResults<Product> oldData;
    private final RealmResults<Product> newData;

    ProductsRealmResultDiffCallback(RealmResults<Product> oldData, RealmResults<Product> newData) {
        this.oldData = oldData;
        this.newData = newData;
    }

    @Override
    public int getOldListSize() {
        return oldData != null && oldData.isValid() ? oldData.size() : 0;
    }

    @Override
    public int getNewListSize() {
        return newData != null && newData.isValid() ? newData.size() : 0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Product oldP = oldData.get(oldItemPosition);
        Product newP = newData.get(newItemPosition);

        if (oldP == null) {
            return newP == null;
        } else {
            return newP != null && oldP.getId() == newP.getId();
        }
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return Objects.equals(oldData.get(oldItemPosition), newData.get(newItemPosition));
    }
}

