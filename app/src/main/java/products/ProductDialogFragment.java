package products;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zpi.mobileshop.R;
import com.zpi.mobileshop.databinding.FragmentProductDialogBinding;

import main.basket.BasketViewModel;

public class ProductDialogFragment extends DialogFragment {
    private FragmentProductDialogBinding binding;
    private Product product;
    private BasketViewModel basketViewModel;

    public void setParameters(Product product, BasketViewModel basketViewModel) {
        this.product = product;
        this.basketViewModel = basketViewModel;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                LayoutInflater.from(getContext()),
                R.layout.fragment_product_dialog,
                null,
                false
        );

        binding.setViewModel(basketViewModel);
        binding.setProduct(product);

        basketViewModel.getProductDialogRefreshLiveEvent().observe(this, Void -> binding.setProduct(product));

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        basketViewModel.getProductDialogRefreshLiveEvent().removeObservers(this);
        super.onDestroyView();
    }
}
