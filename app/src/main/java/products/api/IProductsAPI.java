package products.api;

import java.util.List;

import io.reactivex.Observable;
import products.Product;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IProductsAPI {
    @GET("api/products")
    Observable<List<Product>> getAllProducts();

    @GET("api/products/afterUpdateDate/{updateDate}")
    Observable<List<Product>> getAllProducts(@Path("updateDate") String date);
}
