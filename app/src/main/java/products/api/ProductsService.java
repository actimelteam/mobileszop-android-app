package products.api;

import extra.RealmLiveData;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import products.Product;

@SuppressWarnings("unused")
public class ProductsService {
    public static RealmLiveData<Product> getProductsLiveData(Realm realm, Long[] categories) {
        RealmResults<Product> results = realm
                .where(Product.class)
                .in("categoryId", categories)
                .sort("name", Sort.ASCENDING)
                .findAllAsync();

        return new RealmLiveData<>(results);
    }

    public static RealmResults<Product> findProductsLike(Realm realm, String[] criteria) {
        RealmQuery<Product> where = realm.where(Product.class);

        for (String criterium : criteria) {
            where = where.contains("name", criterium, Case.INSENSITIVE);
        }

        return where.findAllAsync();
    }
}
