package orders.api;

import java.util.List;

@SuppressWarnings("unused")
public class LoadedOrderDto {
    private long id;
    private String submissionDate;
    private String plannedCollectionDate;
    private String status;
    private List<OrderPositionDto> orderPositionsSet;

    public long getId() {
        return id;
    }

    public String getSubmissionDate() {
        return submissionDate;
    }

    public String getPlannedCollectionDate() {
        return plannedCollectionDate;
    }

    public String getStatus() {
        return status;
    }

    public List<OrderPositionDto> getOrderPositionsSet() {
        return orderPositionsSet;
    }

    @Override
    public String toString() {
        return "LoadedOrderDto{" +
                "id=" + id +
                ", submissionDate='" + submissionDate + '\'' +
                ", plannedCollectionDate='" + plannedCollectionDate + '\'' +
                ", status='" + status + '\'' +
                ", orderPositionsSet=" + orderPositionsSet +
                '}';
    }
}
