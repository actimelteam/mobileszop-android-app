package orders.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IPlaceOrderAPI {
    @POST("api/orders/addOrder")
    Call<Void> placeOrder(@Body PlacedOrderDto order);
}
