package orders.api;

@SuppressWarnings("unused")
public class OrderPositionDto {
    private long productId;
    private float amount;
    private double unitPrice;

    public OrderPositionDto(long productId, float amount, double unitPrice) {
        this.productId = productId;
        this.amount = amount;
        this.unitPrice = unitPrice;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public String toString() {
        return "OrderPositionDto{" +
                "productId=" + productId +
                ", amount=" + amount +
                ", unitPrice=" + unitPrice +
                '}';
    }
}
