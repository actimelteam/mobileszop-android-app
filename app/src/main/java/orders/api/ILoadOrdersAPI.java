package orders.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

interface ILoadOrdersAPI {
    @GET("api/orders/{token}")
    Call<List<LoadedOrderDto>> getOrders(@Path("token") String email);
}
