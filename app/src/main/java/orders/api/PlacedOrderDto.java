package orders.api;

import java.util.List;

@SuppressWarnings("unused")
public class PlacedOrderDto {
    private String userToken;
    private List<OrderPositionDto> orderPositionsDtoSet;
    private String submissionDate;
    private String plannedCollectionDate;

    public PlacedOrderDto(String userToken, List<OrderPositionDto> orderPositionsDtoSet, String submissionDate, String plannedCollectionDate) {
        this.userToken = userToken;
        this.orderPositionsDtoSet = orderPositionsDtoSet;
        this.submissionDate = submissionDate;
        this.plannedCollectionDate = plannedCollectionDate;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public List<OrderPositionDto> getOrderPositionsDtoSet() {
        return orderPositionsDtoSet;
    }

    public void setOrderPositionsDtoSet(List<OrderPositionDto> orderPositionsDtoSet) {
        this.orderPositionsDtoSet = orderPositionsDtoSet;
    }

    public String getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(String submissionDate) {
        this.submissionDate = submissionDate;
    }

    public String getPlannedCollectionDate() {
        return plannedCollectionDate;
    }

    public void setPlannedCollectionDate(String plannedCollectionDate) {
        this.plannedCollectionDate = plannedCollectionDate;
    }

    @Override
    public String toString() {
        return "PlacedOrderDto{" +
                "orderPositionsDtoSet=" + orderPositionsDtoSet +
                ", submissionDate='" + submissionDate + '\'' +
                ", plannedCollectionDate='" + plannedCollectionDate + '\'' +
                '}';
    }
}
