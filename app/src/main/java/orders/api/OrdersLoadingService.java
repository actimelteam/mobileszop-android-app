package orders.api;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.zpi.mobileshop.BuildConfig;

import java.security.InvalidParameterException;
import java.util.List;

import extra.GoogleSignInHelper;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrdersLoadingService {
    private final ILoadOrdersAPI loadOrdersAPI;

    public OrdersLoadingService() {
        loadOrdersAPI = new Retrofit.Builder()
                .baseUrl(BuildConfig.WebAPIAddress)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ILoadOrdersAPI.class);
    }

    public Observable<Response<List<LoadedOrderDto>>> getOrdersAsync() {
        return Observable.fromCallable(() -> {
            GoogleSignInAccount account = GoogleSignInHelper.getInstance().getCurrentAccount();
            String token = account.getIdToken();

            if (token == null) {
                throw new InvalidParameterException("Not signed in to Google.");
            }

            Call<List<LoadedOrderDto>> call = loadOrdersAPI.getOrders(token);
            return call.execute();
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
