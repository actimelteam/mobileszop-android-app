package orders;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Pair;

import com.zpi.mobileshop.R;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;

import extra.StringUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import orders.api.LoadedOrderDto;
import orders.api.OrderPositionDto;
import products.Product;

public class Order {
    @SuppressWarnings("unused")
    protected long id;
    private Status status;
    private Date submissionDate;
    private Date plannedCollectionDate;
    private LinkedList<Pair<Long, Float>> orderedPositions;

    Order() {
    }

    Order(LoadedOrderDto loadedOrderDto) {
        //noinspection SpellCheckingInspection
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

        id = loadedOrderDto.getId();

        try {
            submissionDate = format.parse(loadedOrderDto.getSubmissionDate());
        } catch (ParseException exception) {
            submissionDate = null;
        }

        try {
            plannedCollectionDate = format.parse(loadedOrderDto.getPlannedCollectionDate());
        } catch (ParseException exception) {
            plannedCollectionDate = null;
        }

        setStatus(loadedOrderDto.getStatus());
        orderedPositions = new LinkedList<>();

        for (OrderPositionDto orderPositionDto : loadedOrderDto.getOrderPositionsSet()) {
            orderedPositions.add(new Pair<>(orderPositionDto.getProductId(), orderPositionDto.getAmount()));
        }
    }

    public Status getStatus() {
        return status;
    }

    private void setStatus(String status) {
        this.status = Status.valueOf(status);
    }

    public Date getSubmissionDate() {
        return submissionDate;
    }

    public String getSubmissionDateString() {
        return StringUtils.capitalizeFirstLetter(new PrettyTime().format(submissionDate));
    }

    private String getStatusString(Context context) {
        switch (status) {
            case SUBMITTED: {
                return context.getString(R.string.orders_order_status_submitted);
            }

            case READY_TO_COLLECT: {
                return context.getString(R.string.orders_order_status_ready);
            }

            case REALISED: {
                return context.getString(R.string.orders_order_status_realised);
            }

            case CANCELED: {
                return context.getString(R.string.orders_order_status_cancelled);
            }
        }

        return context.getString(R.string.orders_order_status_unknown);
    }

    public Observable<Spannable> getOrderDetailsAsync(Context context) {
        return Observable.fromCallable(() -> {
            Realm realmInst = Realm.getDefaultInstance();
            short itemNumber = 0;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Collections.sort(orderedPositions, (o1, o2) -> o2.second.compareTo(o1.second));

            //Łańcuchy znaków
            StringBuilder result;
            String primaryStatusString = getStatusString(context);
            String pauseString = " – ";
            StringBuilder italicProductsString = new StringBuilder();
            String orderDateLabel = context.getString(R.string.orders_order_date) + ' ';
            String primaryOrderDate;
            String orderSubmissionTimeLabel = context.getString(R.string.orders_order_submission_date) + ' ';
            String primarySubmissionTime;
            String orderCollectionLabel = context.getString(R.string.orders_order_planned_collection_date) + ' ';
            String primaryCollectionTime;

            //Przygotowanie listy produktów

            for (Pair<Long, Float> position : orderedPositions) {
                Product product = realmInst.where(Product.class).equalTo("id", position.first).findFirst();
                float amount = position.second;

                if (product != null) {
                    if (itemNumber == 1) {
                        italicProductsString.append("…\n");
                    } else if (itemNumber > 1) {
                        italicProductsString.append('\n');
                    }

                    itemNumber++;

                    if (amount == (int) amount) {
                        italicProductsString.append((int) amount);
                    } else {
                        italicProductsString.append(amount);
                    }

                    italicProductsString.append(" × ");
                    italicProductsString.append(product.getName());
                }
            }
            italicProductsString.append('\n');

            //Przygotowanie daty i czasu

            primaryOrderDate = dateFormat.format(submissionDate);
            primarySubmissionTime = timeFormat.format(submissionDate);
            primaryCollectionTime = timeFormat.format(plannedCollectionDate);

            result = new StringBuilder();
            result.append(primaryStatusString);
            result.append(pauseString);
            result.append(italicProductsString);
            result.append(orderDateLabel);
            result.append(primaryOrderDate);
            result.append(orderSubmissionTimeLabel);
            result.append(primarySubmissionTime);
            result.append(orderCollectionLabel);
            result.append(primaryCollectionTime);


            String resultString = result.toString();
            Spannable spannable = new SpannableString(resultString);
            int primaryTextColor = context.getResources().getColor(R.color.colorPrimaryText);

            int start;
            int end;

            //wyróżnienie dla statusu
            start = 0;
            end = primaryStatusString.length();
            spannable.setSpan(new ForegroundColorSpan(primaryTextColor), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            //kursywa dla produktów
            start = end + pauseString.length();
            end = start + italicProductsString.length();
            spannable.setSpan(new StyleSpan(Typeface.ITALIC), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            //wyróżnienie dla daty zamówienia
            start = end + orderDateLabel.length();
            end = start + primaryOrderDate.length();
            spannable.setSpan(new ForegroundColorSpan(primaryTextColor), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            //wyróżnienie dla godziny złożenia
            start = end + orderSubmissionTimeLabel.length();
            end = start + primarySubmissionTime.length();
            spannable.setSpan(new ForegroundColorSpan(primaryTextColor), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            //wyróżnienie dla planowanej godziny odebrania
            start = end + orderCollectionLabel.length();
            end = start + primaryCollectionTime.length();
            spannable.setSpan(new ForegroundColorSpan(primaryTextColor), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            return spannable;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<String> getSubTotalAsync(Context context) {
        return Observable.fromCallable(() -> {
            Realm realmInst = Realm.getDefaultInstance();
            String resultText;
            double subTotal = 0.0;

            for (Pair<Long, Float> position : orderedPositions) {
                Product product = realmInst.where(Product.class).equalTo("id", position.first).findFirst();
                float amount = position.second;

                if (product != null) {
                    subTotal += product.getPrice() * amount;
                }
            }

            resultText = String.format(Locale.getDefault(), context.getString(R.string.subtotal_string_currency), subTotal);
            return resultText;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;
        return id == order.id && submissionDate.equals(order.submissionDate) && plannedCollectionDate.equals(order.plannedCollectionDate) && status == order.status;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + submissionDate.hashCode();
        result = 31 * result + plannedCollectionDate.hashCode();
        result = 31 * result + status.hashCode();
        return result;
    }

    public enum Status {
        SUBMITTED, READY_TO_COLLECT, REALISED, CANCELED
    }
}
