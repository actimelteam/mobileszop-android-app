package orders;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import extra.GoogleSignInHelper;
import extra.SingleLiveEvent;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;
import orders.api.LoadedOrderDto;
import orders.api.OrdersLoadingService;
import retrofit2.Response;

@SuppressWarnings("WeakerAccess")
public class OrdersViewModel extends AndroidViewModel {
    private final Realm realm;

    private final SingleLiveEvent<Void> notSignedInLiveData = new SingleLiveEvent<>();
    private final MutableLiveData<Void> noOrdersLiveData = new MutableLiveData<>();
    private final MutableLiveData<List<Order>> ongoingOrdersLiveData = new MutableLiveData<>();
    private final MutableLiveData<List<Order>> otherOrdersLiveData = new MutableLiveData<>();
    private final MutableLiveData<Integer> ordersDownloadFailureLiveData = new MutableLiveData<>();
    private final MutableLiveData<Throwable> ordersDownloadExceptionLiveData = new MutableLiveData<>();

    private final CompositeDisposable disposable = new CompositeDisposable();

    public OrdersViewModel(@NonNull Application application) {
        super(application);
        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onCleared() {
        disposable.clear();
        realm.close();
        super.onCleared();
    }

    public void loadOrders() {
        OrdersLoadingService service = new OrdersLoadingService();

        if (!GoogleSignInHelper.getInstance().isSignedIn()) {
            getNotSignedInLiveData().call();
            return;
        }

        Observable<Response<List<LoadedOrderDto>>> orders = service.getOrdersAsync();

        disposable.add(
                orders.subscribe(ordersResponse -> {
                    if (ordersResponse.isSuccessful()) {
                        List<LoadedOrderDto> loadedOrderDtos = ordersResponse.body();

                        if (loadedOrderDtos != null) {
                            LinkedList<Order> loadedOrders = new LinkedList<>();

                            for (LoadedOrderDto loadedOrderDto : loadedOrderDtos) {
                                loadedOrders.add(new Order(loadedOrderDto));
                            }

                            postOrders(loadedOrders);
                        }
                    } else {
                        int errorCode = ordersResponse.code();
                        Log.v("OrdersViewModel", "Order list error code: " + errorCode);

                        ordersDownloadFailureLiveData.postValue(errorCode);
                    }
                }, throwable -> {
                    throwable.printStackTrace();

                    ordersDownloadExceptionLiveData.postValue(throwable);
                })
        );
    }

    private void postOrders(LinkedList<Order> loadedOrders) {
        if (loadedOrders == null || loadedOrders.isEmpty()) {
            noOrdersLiveData.postValue(null);
            return;
        }

        LinkedList<Order> ongoingOrders = new LinkedList<>();
        LinkedList<Order> otherOrders = new LinkedList<>();

        for (Order order : loadedOrders) {
            Order.Status status = order.getStatus();

            if (status == Order.Status.SUBMITTED || status == Order.Status.READY_TO_COLLECT) {
                ongoingOrders.add(order);
            } else {
                otherOrders.add(order);
            }
        }

        Collections.sort(ongoingOrders, (o1, o2) -> o2.getSubmissionDate().compareTo(o1.getSubmissionDate()));
        Collections.sort(otherOrders, (o1, o2) -> o2.getSubmissionDate().compareTo(o1.getSubmissionDate()));

        ongoingOrdersLiveData.postValue(ongoingOrders);
        otherOrdersLiveData.postValue(otherOrders);
    }

    public SingleLiveEvent<Void> getNotSignedInLiveData() {
        return notSignedInLiveData;
    }

    public MutableLiveData<Void> getNoOrdersLiveData() {
        return noOrdersLiveData;
    }

    public MutableLiveData<List<Order>> getOngoingOrdersLiveData() {
        return ongoingOrdersLiveData;
    }

    public MutableLiveData<List<Order>> getOtherOrdersLiveData() {
        return otherOrdersLiveData;
    }

    public MutableLiveData<Integer> getOrdersDownloadFailureLiveData() {
        return ordersDownloadFailureLiveData;
    }

    public MutableLiveData<Throwable> getOrdersDownloadExceptionLiveData() {
        return ordersDownloadExceptionLiveData;
    }
}
