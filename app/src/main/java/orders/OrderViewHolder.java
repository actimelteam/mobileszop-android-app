package orders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import com.zpi.mobileshop.R;
import com.zpi.mobileshop.databinding.FragmentOrdersItemBinding;

import at.blogc.android.views.ExpandableTextView;
import io.reactivex.disposables.Disposable;

class OrderViewHolder extends RecyclerView.ViewHolder {
    private final FragmentOrdersItemBinding binding;
    private Disposable ongoingItemsListLoad = null;
    private ExpandableTextView descriptionText;
    private boolean expanded = false;

    OrderViewHolder(FragmentOrdersItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    @Override
    protected void finalize() throws Throwable {
        if (ongoingItemsListLoad != null && !ongoingItemsListLoad.isDisposed()) {
            ongoingItemsListLoad.dispose();
        }

        super.finalize();
    }

    void bind(@NonNull Order item) {
        binding.setOrder(item);

        loadDescription(item);
        loadSubTotal(item);

        binding.executePendingBindings();
    }

    private void loadDescription(Order item) {
        descriptionText = binding.getRoot().findViewById(R.id.statusAndItemsText);

        ongoingItemsListLoad = item.getOrderDetailsAsync(binding.getRoot().getContext()).subscribe(spannable -> {
                    AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
                    animation.setDuration(300);
                    animation.setInterpolator(new AccelerateDecelerateInterpolator());
                    animation.setFillAfter(true);

                    descriptionText.setText(spannable, TextView.BufferType.SPANNABLE);
                    descriptionText.startAnimation(animation);
                }
        );

        descriptionText.setAnimationDuration(200);
        descriptionText.setInterpolator(new AccelerateDecelerateInterpolator());

        binding.getRoot().setOnClickListener(view -> {
            expanded = !expanded;

            if (expanded) {
                descriptionText.expand();

            } else {
                descriptionText.collapse();
            }
        });
    }

    private void loadSubTotal(Order item) {
        TextView subTotalText = binding.getRoot().findViewById(R.id.orderSubTotal);

        ongoingItemsListLoad = item.getSubTotalAsync(binding.getRoot().getContext()).subscribe(spannable -> {
                    AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
                    animation.setDuration(300);
                    animation.setInterpolator(new AccelerateDecelerateInterpolator());
                    animation.setFillAfter(true);

                    subTotalText.setText(spannable, TextView.BufferType.SPANNABLE);
                    subTotalText.startAnimation(animation);
                }
        );
    }
}
