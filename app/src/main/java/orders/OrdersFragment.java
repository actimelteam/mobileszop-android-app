package orders;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.zpi.mobileshop.R;

import java.util.Locale;

public class OrdersFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView ongoingList;
    private RecyclerView othersList;
    private TextView errorMessage;
    private ViewGroup messageLayout;
    private ConstraintLayout ongoingLayout;
    private ConstraintLayout othersLayout;

    private OrdersViewModel ordersViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders, container, false);
        ordersViewModel = ViewModelProviders.of(this).get(OrdersViewModel.class);

        FragmentActivity activity = getActivity();
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        ongoingList = view.findViewById(R.id.ongoingOrdersList);
        othersList = view.findViewById(R.id.otherOrdersList);
        errorMessage = view.findViewById(R.id.errorMessage);
        messageLayout = view.findViewById(R.id.messageLayout);

        ongoingLayout = view.findViewById(R.id.ongoingOrdersLayout);
        othersLayout = view.findViewById(R.id.otherOrdersLayout);

        swipeRefreshLayout.setOnRefreshListener(this);

        ongoingList.setNestedScrollingEnabled(false);
        ongoingList.setLayoutManager(new LinearLayoutManager(activity));
        ongoingList.setAdapter(new OrdersAdapter());

        if (activity != null) {
            ongoingList.addItemDecoration(new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL));
        }

        othersList.setNestedScrollingEnabled(false);
        othersList.setLayoutManager(new LinearLayoutManager(activity));
        othersList.setAdapter(new OrdersAdapter());

        if (activity != null) {
            othersList.addItemDecoration(new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL));
        }

        ordersViewModel.getNotSignedInLiveData().observe(this, aVoid ->
                showErrorMessageText(getString(R.string.orders_not_signed_in))
        );

        ordersViewModel.getNoOrdersLiveData().observe(this, aVoid ->
                showErrorMessageText(getString(R.string.orders_no_orders))
        );

        ordersViewModel.getOngoingOrdersLiveData().observe(this, orders -> {
            if (orders != null && !orders.isEmpty()) {
                ((OrdersAdapter) ongoingList.getAdapter()).updateData(orders);
                setOngoingListVisibility(true);
            } else {
                setOngoingListVisibility(false);
            }
        });

        ordersViewModel.getOtherOrdersLiveData().observe(this, orders -> {
            if (orders != null && !orders.isEmpty()) {
                ((OrdersAdapter) othersList.getAdapter()).updateData(orders);
                setOthersListVisibility(true);
            } else {
                setOthersListVisibility(false);
            }
        });

        ordersViewModel.getOrdersDownloadFailureLiveData().observe(this, errorCode ->
                showErrorMessageText(String.format(Locale.getDefault(), getString(R.string.order_error_code_occurred), errorCode))
        );

        ordersViewModel.getOrdersDownloadExceptionLiveData().observe(this, throwable ->
                showErrorMessageText(getString(R.string.orders_error_occurred))
        );

        ordersViewModel.loadOrders();
        swipeRefreshLayout.setRefreshing(true);
        return view;
    }

    public void refreshData() {
        if (ordersViewModel != null) {
            ordersViewModel.loadOrders();
        }
    }

    private void showErrorMessageText(String text) {
        errorMessage.setText(text);
        messageLayout.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);

        ongoingLayout.setVisibility(View.GONE);
        othersLayout.setVisibility(View.GONE);
    }

    private void setOngoingListVisibility(boolean visibility) {
        swipeRefreshLayout.setRefreshing(false);

        if (ongoingLayout.getVisibility() == View.GONE && visibility) {
            messageLayout.setVisibility(View.GONE);

            ongoingLayout.setVisibility(View.VISIBLE);
            ongoingLayout.setAnimation(getFadeInAnimation());
        } else if (ongoingLayout.getVisibility() == View.VISIBLE && !visibility) {
            ongoingLayout.setVisibility(View.GONE);
            ongoingLayout.setAnimation(getFadeOutAnimation());
        }
    }

    private void setOthersListVisibility(boolean visibility) {
        swipeRefreshLayout.setRefreshing(false);

        if (othersLayout.getVisibility() == View.GONE && visibility) {
            messageLayout.setVisibility(View.GONE);

            othersLayout.setVisibility(View.VISIBLE);
            othersLayout.setAnimation(getFadeInAnimation());
        } else if (othersLayout.getVisibility() == View.VISIBLE && !visibility) {
            othersLayout.setVisibility(View.GONE);
            othersLayout.setAnimation(getFadeOutAnimation());
        }
    }

    @Override
    public void onRefresh() {
        ordersViewModel.loadOrders();
    }

    private Animation getFadeInAnimation() {
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new AccelerateInterpolator());
        fadeIn.setDuration(500);
        return fadeIn;
    }

    private Animation getFadeOutAnimation() {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new DecelerateInterpolator());
        fadeOut.setDuration(500);
        return fadeOut;
    }
}
