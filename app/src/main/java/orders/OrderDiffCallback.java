package orders;

import android.support.v7.util.DiffUtil;

import java.util.List;
import java.util.Objects;

class OrderDiffCallback extends DiffUtil.Callback {
    private final List<Order> oldList;
    private final List<Order> newList;

    OrderDiffCallback(List<Order> oldList, List<Order> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).id == newList.get(newItemPosition).id;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return Objects.equals(oldList.get(oldItemPosition), newList.get(newItemPosition));
    }
}
