package orders;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zpi.mobileshop.R;
import com.zpi.mobileshop.databinding.FragmentOrdersItemBinding;

import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class OrdersAdapter extends RecyclerView.Adapter<OrderViewHolder> {
    private List<Order> data;
    private Disposable ongoingDataUpdate;

    OrdersAdapter() {
        this.data = new LinkedList<>();
    }

    @Override
    protected void finalize() throws Throwable {
        if (ongoingDataUpdate != null && !ongoingDataUpdate.isDisposed()) {
            ongoingDataUpdate.dispose();
        }

        super.finalize();
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        FragmentOrdersItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_orders_item, parent, false);
        return new OrderViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        Order item = data.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void updateData(List<Order> newData) {
        if (ongoingDataUpdate != null) {
            throw new ConcurrentModificationException();
        }

        ongoingDataUpdate =
                Observable.fromCallable(() -> DiffUtil.calculateDiff(new OrderDiffCallback(data, newData)))
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                diffResult -> {
                                    OrdersAdapter.this.data = newData;
                                    diffResult.dispatchUpdatesTo(this);
                                }, Throwable::printStackTrace,
                                () -> ongoingDataUpdate = null
                        );
    }
}
