package onboarding;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Pair;

import extra.GoogleSignInHelper;
import extra.SingleLiveEvent;

@SuppressWarnings("WeakerAccess")
public class OnboardingViewModel extends AndroidViewModel {
    private final SingleLiveEvent<Pair<Intent, Integer>> startIntentEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<String> successEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Integer> failureEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> finishActivityEvent = new SingleLiveEvent<>();

    public OnboardingViewModel(@NonNull Application application) {
        super(application);

        if (GoogleSignInHelper.getInstance().isSignedIn()) {
            finishActivityEvent.call();
        }

        PreferenceManager.getDefaultSharedPreferences(application).edit().putBoolean("OnboardingComplete", true).apply();
    }

    void signInButtonClicked() {
        Pair<Intent, Integer> intentAndResultCode = GoogleSignInHelper.getInstance().startSignIn();
        startIntentEvent.postValue(intentAndResultCode);
    }

    void handleActivityResult(int requestCode, Intent data) {
        GoogleSignInHelper.GoogleSignInStatus result = GoogleSignInHelper.getInstance().handleSignInResult(requestCode, data);

        if (result != null) {
            if (result.status == GoogleSignInHelper.StatusCode.STATUS_OK) {
                successEvent.postValue(result.account.getDisplayName());
            } else {
                failureEvent.postValue(result.errorCode);
            }
        }
    }

    SingleLiveEvent<Pair<Intent, Integer>> getStartIntentEvent() {
        return startIntentEvent;
    }

    SingleLiveEvent<String> getSuccessEvent() {
        return successEvent;
    }

    SingleLiveEvent<Integer> getFailureEvent() {
        return failureEvent;
    }

    SingleLiveEvent<Void> getFinishActivityEvent() {
        return finishActivityEvent;
    }
}
