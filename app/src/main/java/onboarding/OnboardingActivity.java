package onboarding;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.zpi.mobileshop.R;

public class OnboardingActivity extends AppCompatActivity {
    private final OnboardingActivity thisActivity = this;
    private OnboardingViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_onboarding);
        viewModel = ViewModelProviders.of(this).get(OnboardingViewModel.class);

        ActionBar toolbar = getSupportActionBar();

        if (toolbar != null) {
            toolbar.setDisplayHomeAsUpEnabled(true);
            toolbar.setHomeAsUpIndicator(R.drawable.ic_close);
            toolbar.setTitle("");
        }

        viewModel.getStartIntentEvent().observe(this, intentAndResultCode -> {
            if (intentAndResultCode != null) {
                startActivityForResult(intentAndResultCode.first, intentAndResultCode.second);
            }
        });

        viewModel.getSuccessEvent().observe(this, accountName -> {
            Toast.makeText(thisActivity, String.format(getString(R.string.onboarding_toast_signed_in_as), accountName), Toast.LENGTH_LONG).show();
            finish();
        });

        viewModel.getFailureEvent().observe(this, errorCode -> Toast.makeText(thisActivity, String.format(getString(R.string.onboarding_toast_error), errorCode), Toast.LENGTH_LONG).show());

        viewModel.getFinishActivityEvent().observe(this, Void -> finish());

        findViewById(R.id.btnSignIn).setOnClickListener(view -> viewModel.signInButtonClicked());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.handleActivityResult(requestCode, data);
    }
}
