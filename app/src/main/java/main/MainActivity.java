package main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.bottomnavigation.LabelVisibilityMode;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.zpi.mobileshop.R;

import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.Locale;

import categories.CategoriesFragment;
import categories.Category;
import extra.ArrayUtils;
import extra.KeyboardUtil;
import home.HomeFragment;
import main.basket.BasketAdapter;
import main.basket.BasketViewModel;
import main.place_order.OrderTimePicker;
import main.place_order.PlaceOrderViewModel;
import main.search.CategoryResult;
import main.search.ProductResult;
import main.search.SearchResult;
import main.search.SearchResultsAdapter;
import main.search.SearchViewModel;
import onboarding.OnboardingActivity;
import orders.OrdersFragment;
import products.Product;
import products.ProductDialogFragment;
import products.ProductsFragment;
import settings.SettingsFragment;
import splash.api.ShopData;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private final AppCompatActivity thisActivity = this;
    private DrawerLayout drawerLayout;
    private CoordinatorLayout coordinatorLayout;
    private AppBarLayout appBarLayout;
    private Toolbar toolbar;
    private ImageView toolbarHomeButton;
    private EditText searchEditText;

    private SearchViewModel searchViewModel;
    private BasketViewModel basketViewModel;
    private PlaceOrderViewModel placeOrderViewModel;

    private ImageView sortIcon;
    private TextView drawerSubTotal;
    private TextView toolbarSubTotal;
    private FrameLayout container;
    private BottomNavigationView navigation;
    private RecyclerView basketList;
    private TextView openHoursText;
    private ProgressBar orderProgressBar;
    private Button orderItemsButton;
    private ScrollView searchResultsContainer;
    private RecyclerView searchResultsList;

    private int currentNavigationItemId;
    private Fragment currentFragment;
    private HomeFragment homeFragment;
    private CategoriesFragment categoriesFragment;
    private ProductsFragment productsFragment;
    private OrdersFragment ordersFragment;

    private boolean searchViewOpen = false;
    private boolean searchResultOpen = false;
    private boolean blockNavigationItemSelectedEventOnce = false;
    private int previousNavigationItemId = 0;
    private Fragment previousFragment = null;
    private String previousSearchCriteria = null;

    //Przygotowanie aktywności

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        drawerLayout = findViewById(R.id.drawer_layout);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        appBarLayout = findViewById(R.id.appBarLayout);
        toolbar = findViewById(R.id.toolbar);
        toolbarHomeButton = findViewById(R.id.toolbarHomeButton);
        searchEditText = findViewById(R.id.searchEditText);

        sortIcon = findViewById(R.id.sortIcon);
        drawerSubTotal = findViewById(R.id.drawerSubTotal);
        toolbarSubTotal = findViewById(R.id.toolbarSubTotal);

        container = findViewById(R.id.container);
        navigation = findViewById(R.id.navigation);

        basketList = findViewById(R.id.basketList);
        openHoursText = findViewById(R.id.openHoursText);
        orderProgressBar = findViewById(R.id.orderProgressBar);
        orderItemsButton = findViewById(R.id.orderItems);
        searchResultsContainer = findViewById(R.id.searchResultsContainer);
        searchResultsList = findViewById(R.id.searchResultsList);

        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel.class);
        basketViewModel = ViewModelProviders.of(this).get(BasketViewModel.class);
        placeOrderViewModel = ViewModelProviders.of(this).get(PlaceOrderViewModel.class);

        //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-==-=-==-=-= SEARCH =-=-==-=-==-=-==-=-==-=-=-=-=-=

        searchViewModel.getOpenSearchResultLiveData().observe(this, searchResult -> {
            if (searchResult instanceof ProductResult) {
                ProductDialogFragment dialog = new ProductDialogFragment();
                dialog.setParameters((Product) searchResult.getObject(), basketViewModel);
                dialog.show(getSupportFragmentManager(), "PRODUCT");
            } else if (searchResult instanceof CategoryResult) {
                //Ukrycie klawiatury
                KeyboardUtil.hideKeyboard(this);
                searchEditText.clearFocus();

                //Przygotowanie fragmentu z produktami w kategorii
                Category categoryResult = (Category) searchResult.getObject();
                ProductsFragment productsFragment = new ProductsFragment();
                Bundle bundle = new Bundle();
                LinkedList<Long> subcategoryIdsList = categoryResult.getSubcategoriesRecursively();
                long[] subcategoryIdsArray = ArrayUtils.convertListToPrimitiveArray(subcategoryIdsList);

                bundle.putLongArray("categories", subcategoryIdsArray);
                bundle.putString("firstCategoryName", categoryResult.getName());
                productsFragment.setArguments(bundle);

                //Wczytanie
                searchResultOpen = true;
                blockNavigationItemSelectedEventOnce = true;
                previousNavigationItemId = currentNavigationItemId;
                previousFragment = currentFragment;
                previousSearchCriteria = searchEditText.getText().toString();
                closeSearchView();
                navigation.setSelectedItemId(R.id.nav_products);
                loadFragment(productsFragment, true);
            }
        });

        //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-==-=-==-=-= BASKET =-=-==-=-==-=-==-=-==-=-=-=-=-=

        basketViewModel.getSubTotalLiveData().observe(this, subTotal -> {
            if (subTotal != null) {
                String toolbarString = String.format(Locale.getDefault(), getString(R.string.subtotal_string), subTotal);
                String drawerString = String.format(Locale.getDefault(), getString(R.string.subtotal_string_currency), subTotal);

                toolbarSubTotal.setText(toolbarString);
                drawerSubTotal.setText(drawerString);

                findViewById(R.id.orderItems).setEnabled(subTotal != 0.00); //blokada przycisku jeśli koszyk jest pusty
            }
        });

        //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-==-=-==-=-= PLACE ORDER =-=-==-=-==-=-==-=-=-=-=-=

        placeOrderViewModel.getNeedToSignInLiveData().observe(this, aVoid -> {
                    orderItemsButton.setEnabled(true);
                    startActivity(new Intent(this, OnboardingActivity.class));
                }
        );

        placeOrderViewModel.getOrderDismissMutableLiveData().observe(this, aVoid ->
                orderItemsButton.setEnabled(true)
        );

        placeOrderViewModel.getOrderConfirmationDialogMutableLiveData().observe(this, orderConfirmationDialog -> {
            if (orderConfirmationDialog != null) {
                orderConfirmationDialog.show(getSupportFragmentManager(), "ORDER_CONFIRMATION");
            }
        });

        placeOrderViewModel.getPlacingOrderInProgressLiveData().observe(this, inProgress -> {
                    if (inProgress != null) {
                        orderProgressBar.setVisibility(inProgress ? View.VISIBLE : View.INVISIBLE);
                    }
                }
        );

        placeOrderViewModel.getSuccessLiveData().observe(this, aVoid -> {
            Snackbar.make(coordinatorLayout, R.string.main_place_order_success, Snackbar.LENGTH_LONG).show();

                    navigation.setSelectedItemId(R.id.nav_orders);
                    ordersFragment.refreshData();
                    closeDrawer();
                }
        );

        placeOrderViewModel.getFailureLiveData().observe(this, errorCode -> {
            Snackbar.make(drawerLayout, String.format(Locale.getDefault(), getString(R.string.main_place_order_error), errorCode), Snackbar.LENGTH_LONG).show();

            orderItemsButton.setEnabled(true);
        });

        placeOrderViewModel.getExceptionLiveData().observe(this, Void -> {
            orderItemsButton.setEnabled(true);
            Snackbar.make(drawerLayout, R.string.main_place_order_error2, Snackbar.LENGTH_LONG).show();
        });

        prepareToolbarAndDrawer();

        navigation.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        navigation.setOnNavigationItemSelectedListener(this);
        loadFragment(R.id.nav_home);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (!sharedPreferences.getBoolean("OnboardingComplete", false)) {
            startActivity(new Intent(this, OnboardingActivity.class));
        }
    }

    private void prepareToolbarAndDrawer() {
        setSupportActionBar(toolbar);
        prepareSearchButton();

        BasketAdapter adapter = new BasketAdapter(this, basketViewModel);
        DefaultItemAnimator animator = new DefaultItemAnimator();
        animator.setSupportsChangeAnimations(false);

        basketList.setHasFixedSize(true);
        basketList.setLayoutManager(new LinearLayoutManager(this));
        basketList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        basketList.setItemAnimator(animator);
        basketList.setAdapter(adapter);

        ShopData shopData = ShopData.getInstance();

        openHoursText.setText(
                String.format(
                        Locale.getDefault(),
                        getString(R.string.orders_time_limitations),
                        shopData.getEndOrderingTime(),
                        shopData.getStartCollectingTime(),
                        shopData.getEndCollectingTime()
                )
        );
    }

    private void prepareSearchButton() {
        searchResultsList.setLayoutManager(new LinearLayoutManager(thisActivity));
        searchResultsList.addItemDecoration(new DividerItemDecoration(thisActivity, DividerItemDecoration.VERTICAL));

        searchEditText.setOnFocusChangeListener((view, hasFocus) -> {
            if (hasFocus) {
                openSearchView();
            }
        });

        searchEditText.setOnClickListener(view -> openSearchView());

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable text) {
                searchTextChanged(text.toString());
            }
        });
    }

    private void loadFragment(int fragmentId) {
        if (fragmentId == currentNavigationItemId && fragmentId == R.id.nav_settings) {
            return;
        }

        Fragment fragmentToLoad;
        currentNavigationItemId = fragmentId;

        switch (fragmentId) {
            case R.id.nav_home: {
                fragmentToLoad =
                        homeFragment != null
                                ? homeFragment
                                : (homeFragment = new HomeFragment());
                break;
            }

            case R.id.nav_products: {
                if (productsFragment == null) {
                    fragmentToLoad =
                            categoriesFragment != null
                                    ? categoriesFragment
                                    : (categoriesFragment = new CategoriesFragment());
                } else {
                    fragmentToLoad = productsFragment;
                }
                break;
            }

            case R.id.nav_orders: {
                fragmentToLoad =
                        ordersFragment != null
                                ? ordersFragment
                                : (ordersFragment = new OrdersFragment());
                break;
            }

            case R.id.nav_settings: {
                fragmentToLoad = new SettingsFragment();
                break;
            }

            default: {
                throw new InvalidParameterException("Trying to getOpenFragment non-existing fragment.");
            }
        }

        loadFragment(fragmentToLoad);
    }

    public void loadFragment(Fragment fragmentToLoad) {
        loadFragment(fragmentToLoad, false);
    }

    private void loadFragment(Fragment fragmentToLoad, boolean openSearchResults) {
        appBarLayout.setExpanded(true, true);
        navigation.setTranslationY(0);

        if (fragmentToLoad instanceof ProductsFragment) {
            if (!openSearchResults) {
                productsFragment = (ProductsFragment) fragmentToLoad;
            }

            String firstCategoryName = ((ProductsFragment) fragmentToLoad).getFirstCategoryName();
            searchEditText.setHint(String.format(Locale.getDefault(), getString(R.string.products_search_in), firstCategoryName));

            showSortIcon();
            toolbarHomeButton.setImageDrawable(getDrawable(R.drawable.search_bar_arrow_back));
        } else {
            searchEditText.setHint(R.string.basket_search_in_app);
            hideSortIcon();
            toolbarHomeButton.setImageDrawable(getDrawable(R.drawable.search_bar_magnify));
        }

        currentFragment = fragmentToLoad;

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.container, fragmentToLoad)
                .commit();
    }

    //Wydarzenia

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (blockNavigationItemSelectedEventOnce) {
            blockNavigationItemSelectedEventOnce = false;
        } else {
            loadFragment(item.getItemId());
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (goToPreviousViewIfPossible()) {
            return;
        }

        super.onBackPressed();
    }

    public void clickToolbarHomeButton(@SuppressWarnings("unused") View view) {
        if (!goToPreviousViewIfPossible()) {
            openSearchViewAndIme();
        }
    }

    public void clickSearchBox(@SuppressWarnings("unused") View view) {
        if (!searchViewOpen) {
            openSearchViewAndIme();
        }
    }

    public void clickBasketIcon(@SuppressWarnings("unused") View view) {
        drawerLayout.openDrawer(Gravity.END);
    }

    public void clickOrderItemsButton(@SuppressWarnings("unused") View view) {
        OrderTimePicker picker = placeOrderViewModel.openTimePicker(this, basketViewModel);

        if (picker != null) {
            view.setEnabled(false);
            picker.show(getSupportFragmentManager(), "timePicker");
        }
    }

    public void clickSortIcon(@SuppressWarnings("unused") View view) {
        if (productsFragment != null) {
            productsFragment.openSortDialog();
        }
    }

    public void clickShopInfoBox(@SuppressWarnings("unused") View view) {
        ShopInfoDialog dialog = new ShopInfoDialog();
        dialog.show(getSupportFragmentManager(), "shopInfo");
    }

    //Metody pomocnicze

    private void openSearchViewAndIme() {
        InputMethodManager imeManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        searchEditText.requestFocus();

        if (imeManager != null) {
            imeManager.showSoftInput(searchEditText, InputMethodManager.SHOW_IMPLICIT);
        }

        openSearchView();
    }

    private void openSearchView() {
        searchViewOpen = true;

        if (!(currentFragment instanceof ProductsFragment)) {
            container.setVisibility(View.GONE);
            navigation.setVisibility(View.GONE);
            searchResultsContainer.setVisibility(View.VISIBLE);

            SearchResultsAdapter adapter = new SearchResultsAdapter();
            searchResultsList.setAdapter(adapter);

            searchViewModel.getSearchResultLiveData().observe(thisActivity, searchResults -> {
                if (searchResults != null) {
                    adapter.updateData(searchResults.toArray(new SearchResult[searchResults.size()]));
                } else {
                    adapter.updateData(new SearchResult[]{});
                }
            });
        }

        toolbarHomeButton.setImageDrawable(getDrawable(R.drawable.search_bar_arrow_back));
    }

    private void searchTextChanged(String criteria) {
        if (currentFragment instanceof ProductsFragment) {
            ((ProductsFragment) currentFragment).setSearchCriteria(criteria.split(" "));
        } else {
            searchViewModel.updateSearchResults(criteria);
        }
    }

    private void closeSearchView() {
        searchViewOpen = false;

        KeyboardUtil.hideKeyboard(this);

        if (currentFragment instanceof ProductsFragment) {
            productsFragment.setSearchCriteria(null);
        } else {
            container.setVisibility(View.VISIBLE);
            navigation.setVisibility(View.VISIBLE);
            searchResultsContainer.setVisibility(View.GONE);
            searchResultsList.removeAllViewsInLayout();
        }

        searchEditText.setText(null);
        toolbarHomeButton.setImageDrawable(getDrawable(R.drawable.search_bar_magnify));

        searchViewModel.getSearchResultLiveData().removeObservers(thisActivity);
    }

    private void openSearchViewWithCriteria(String criteria) {
        openSearchView();
        searchEditText.setText(criteria);
        searchEditText.requestFocus();
    }

    private void showSortIcon() {
        ConstraintLayout layout = (ConstraintLayout) searchEditText.getParent();
        ConstraintSet set = new ConstraintSet();

        sortIcon.setVisibility(View.VISIBLE);
        set.clone(layout);
        set.connect(R.id.searchEditText, ConstraintSet.END, R.id.sortIcon, ConstraintSet.START);
        set.applyTo(layout);
    }

    private void hideSortIcon() {
        ConstraintLayout layout = (ConstraintLayout) searchEditText.getParent();
        ConstraintSet set = new ConstraintSet();

        sortIcon.setVisibility(View.GONE);
        set.clone(layout);
        set.connect(R.id.searchEditText, ConstraintSet.END, R.id.toolbarSubTotal, ConstraintSet.START);
        set.applyTo(layout);
    }

    private boolean goToPreviousViewIfPossible() {
        if (isDrawerOpen()) {
            closeDrawer();
            return true;
        }

        if (searchResultOpen) {
            searchResultOpen = false;
            blockNavigationItemSelectedEventOnce = true;
            navigation.setSelectedItemId(previousNavigationItemId);
            loadFragment(previousFragment);
            openSearchViewWithCriteria(previousSearchCriteria);
            return true;
        }

        if (searchViewOpen) {
            closeSearchView();
            return true;
        }

        if (currentFragment instanceof ProductsFragment) {
            productsFragment = null;
            loadFragment(R.id.nav_products);
            return true;
        }

        return false;
    }

    public void closeBasketDrawer(@SuppressWarnings("unused") View view) {
        if (isDrawerOpen()) {
            closeDrawer();
        }
    }

    private boolean isDrawerOpen() {
        return drawerLayout.isDrawerOpen(GravityCompat.END);
    }

    private void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.END);
    }
}
