package main.place_order;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayout;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.zpi.mobileshop.R;

import java.security.InvalidParameterException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Objects;

import extra.Time;
import splash.api.ShopData;

public class OrderTimePicker extends DialogFragment {
    private boolean collectTomorrow = false;
    private Time timeRangeFrom;
    private Time timeRangeTo;

    private AlertDialog dialog;

    private TextView displayHour;
    private TextView displayMinute;
    private ConstraintLayout errorMessageBlock;

    private TextView pickedGridHour;
    private TextView pickedGridMinute;

    private TimePickerResponseListener listener;

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        FragmentActivity activity = Objects.requireNonNull(getActivity());

        ShopData shopData = ShopData.getInstance();
        Time endOrderingTime = shopData.getEndOrderingTime();
        Time startCollectingTime = shopData.getStartCollectingTime();
        Time endCollectingTime = shopData.getEndCollectingTime();
        Time now = Time.now();

        timeRangeTo = endCollectingTime;

        if (endOrderingTime == null || startCollectingTime == null || endCollectingTime == null) {
            throw new InvalidParameterException("No information about time range to be shown in picker.");
        }

        if (now.isGreaterThan(endOrderingTime)) {
            timeRangeFrom = startCollectingTime; //za późno, trzeba będzie jutro odebrać
            collectTomorrow = true;
        } else if (now.isGreaterThan(startCollectingTime)) {
            timeRangeFrom = now; //można jeszcze zamówić, ale zaczęły się już odbiory
        } else {
            timeRangeFrom = startCollectingTime; //jeszcze nie zaczął się czas odbiorów zamówień
        }

        if (timeRangeFrom.minute % 15 != 0) { //dopełnienie do 15 minut
            timeRangeFrom.minute -= timeRangeFrom.minute % 15;
            timeRangeFrom.minute += 15;

            if (timeRangeFrom.minute == 60) { //przesunięcie do następnej godziny w razie potrzeby
                timeRangeFrom.hour++;
                timeRangeFrom.minute = 0;
            }

            if (timeRangeFrom.isGreaterThan(endCollectingTime)) { //oj, jednak nie zdąży odebrać
                timeRangeFrom = startCollectingTime;
                collectTomorrow = true;
            }
        }

        View layout = activity.getLayoutInflater().inflate(R.layout.activity_base_time_picker, null);
        displayHour = layout.findViewById(R.id.pickedHour);
        displayMinute = layout.findViewById(R.id.pickedMinute);
        errorMessageBlock = layout.findViewById(R.id.errorMessageBlock);

        TextView pickerMessage = layout.findViewById(R.id.pickerMessage);
        ConstraintLayout orderTomorrowWarning = layout.findViewById(R.id.orderTomorrowWarning);
        GridLayout hourOptionsGrid = layout.findViewById(R.id.hourOptionsGrid);
        GridLayout minuteOptionsGrid = layout.findViewById(R.id.minuteOptionsGrid);
        int rippleResourceId = getRippleResourceId(activity);

        pickerMessage.setText(
                String.format(
                        Locale.getDefault(),
                        getString(R.string.alert_message),
                        timeRangeFrom.toString(),
                        timeRangeTo.toString()
                ));

        orderTomorrowWarning.setVisibility(collectTomorrow ? View.VISIBLE : View.GONE);
        displayHour.setText(String.valueOf(timeRangeFrom));
        displayMinute.setText(String.valueOf(timeRangeFrom.minute));

        for (int hour = timeRangeFrom.hour; hour <= timeRangeTo.hour; hour++) {
            hourOptionsGrid.addView(
                    createHourTextView(
                            hour,
                            rippleResourceId,
                            hour == timeRangeFrom.hour
                    )
            );
        }

        for (int minute = 0; minute < 60; minute += 15) {
            minuteOptionsGrid.addView(
                    createMinuteTextView(
                            minute,
                            rippleResourceId,
                            minute == timeRangeFrom.minute
                    )

            );
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.alert_picker_header);
        builder.setView(layout);

        builder.setPositiveButton(R.string.alert_ok, (dialog, id) -> {
            Calendar calendarNow = Calendar.getInstance();

                    GregorianCalendar date = new GregorianCalendar(
                            calendarNow.get(Calendar.YEAR),
                            calendarNow.get(Calendar.MONTH),
                            calendarNow.get(Calendar.DAY_OF_MONTH),
                            Integer.parseInt(displayHour.getText().toString()),
                            Integer.parseInt(displayMinute.getText().toString())
                    );

            if (collectTomorrow) {
                date.add(Calendar.DAY_OF_MONTH, 1);
            }

                    listener.accept(date);
                }
        );

        builder.setNegativeButton(R.string.alert_cancel, (dialog, id) ->
                listener.cancel()
        );

        dialog = builder.create();
        return dialog;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        if (listener != null) {
            listener.cancel();
        }

        super.onCancel(dialog);
    }

    public void setListener(TimePickerResponseListener listener) {
        this.listener = listener;
    }

    private int getRippleResourceId(FragmentActivity activity) {
        TypedValue clickability = new TypedValue();
        activity.getTheme().resolveAttribute(R.attr.actionBarItemBackground, clickability, true);
        return clickability.resourceId;
    }

    private TextView createHourTextView(int hour, int rippleResourceId, boolean defaultOption) {
        TextView hourText = new TextView(getContext());

        hourText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
        hourText.setText(String.valueOf(hour));
        hourText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
        hourText.setBackgroundResource(rippleResourceId);

        hourText.setOnClickListener(view ->
                clickHourOption(((TextView) view))
        );

        if (defaultOption) {
            clickHourOption(hourText);
        } else {
            hourText.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        }

        hourText.setPadding(32, 8, 32, 8);
        return hourText;
    }

    private TextView createMinuteTextView(int minute, int rippleResourceId, boolean defaultOption) {
        TextView minuteText = new TextView(getContext());

        minuteText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
        minuteText.setText(String.format(Locale.getDefault(), "%02d", minute));
        minuteText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
        minuteText.setBackgroundResource(rippleResourceId);

        if (defaultOption) {
            clickMinuteOption(minuteText);
        } else {
            minuteText.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        }

        minuteText.setOnClickListener(view ->
                clickMinuteOption(((TextView) view))
        );

        minuteText.setPadding(8, 8, 8, 8);
        return minuteText;
    }

    private void clickHourOption(TextView view) {
        if (pickedGridHour == view) {
            return;
        }

        CharSequence hour = view.getText();

        if (pickedGridHour != null) {
            pickedGridHour.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        }

        displayHour.setText(hour);
        view.setTypeface(Typeface.create("sans-serif-light", Typeface.BOLD));
        pickedGridHour = view;
        checkCurrentTime();
    }

    private void clickMinuteOption(TextView view) {
        if (pickedGridMinute == view) {
            return;
        }

        CharSequence minute = view.getText();

        if (pickedGridMinute != null) {
            pickedGridMinute.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        }

        displayMinute.setText(minute);
        view.setTypeface(Typeface.create("sans-serif-light", Typeface.BOLD));
        pickedGridMinute = view;
        checkCurrentTime();
    }

    private void checkCurrentTime() {
        if (dialog != null && displayHour != null && displayMinute != null) {
            int currentHour = Short.parseShort(String.valueOf(displayHour.getText()));
            int currentMinute = Short.parseShort(String.valueOf(displayMinute.getText()));
            Time currentTime = new Time(currentHour, currentMinute);

            boolean correct = currentTime.isInRange(timeRangeFrom, timeRangeTo);

            errorMessageBlock.setVisibility(correct ? View.INVISIBLE : View.VISIBLE);
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(correct);
        }
    }

    public interface TimePickerResponseListener {
        void accept(GregorianCalendar collectionDate);

        void cancel();
    }
}
