package main.place_order;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.zpi.mobileshop.R;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import extra.Tuple4;

public class OrderConfirmationDialog extends DialogFragment {
    private String clientName;
    private String submissionDateString;
    private String plannedCollectionDateString;
    private String totalSumString;
    private List<Tuple4<String, String, String, String>> tableData;
    private ConfirmationResponseListener listener;

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setSubmissionDateString(String submissionDateString) {
        this.submissionDateString = submissionDateString;
    }

    public void setPlannedCollectionDateString(String plannedCollectionDateString) {
        this.plannedCollectionDateString = plannedCollectionDateString;
    }

    public void setTotalSumString(String totalSumString) {
        this.totalSumString = totalSumString;
    }

    public void setTableData(List<Tuple4<String, String, String, String>> tableData) {
        this.tableData = tableData;
    }

    public void setListener(ConfirmationResponseListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        FragmentActivity activity = Objects.requireNonNull(getActivity());

        if (clientName == null || submissionDateString == null
                || plannedCollectionDateString == null || totalSumString == null
                || tableData == null
                || listener == null) {
            throw new InvalidParameterException("One of confirmation dialog data hasn't been set.");
        }

        @SuppressLint("InflateParams")
        View layout = activity.getLayoutInflater().inflate(R.layout.activity_base_order_confirmation, null);
        TextView clientText = layout.findViewById(R.id.clientText);
        TextView submissionDateText = layout.findViewById(R.id.submissionDateText);
        TextView plannedCollectionDateText = layout.findViewById(R.id.plannedCollectionDateText);
        TextView totalSumText = layout.findViewById(R.id.totalSumText);
        TableLayout orderedProducts = layout.findViewById(R.id.orderedProducts);
        Context context = getContext();

        clientText.setText(clientName);
        submissionDateText.setText(submissionDateString);
        plannedCollectionDateText.setText(plannedCollectionDateString);
        totalSumText.setText(totalSumString);

        int primaryTextColor = getResources().getColor(R.color.colorPrimaryText);

        for (Tuple4<String, String, String, String> row : tableData) {
            TableRow tableRow = new TableRow(context);

            TextView nameCell = new TextView(context);
            nameCell.setText(row.first);
            nameCell.setTextColor(primaryTextColor);
            nameCell.setPadding(16, 16, 16, 16);
            tableRow.addView(nameCell);

            TextView amountAndUnitPriceCell = new TextView(context);
            amountAndUnitPriceCell.setText(
                    String.format(Locale.getDefault(), "%s\n × %s", row.second, row.third)
            );
            amountAndUnitPriceCell.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            amountAndUnitPriceCell.setTextColor(primaryTextColor);
            amountAndUnitPriceCell.setPadding(16, 16, 16, 16);
            tableRow.addView(amountAndUnitPriceCell);


            TextView priceCell = new TextView(context);
            priceCell.setText(row.fourth);
            priceCell.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            priceCell.setTextColor(primaryTextColor);
            priceCell.setPadding(16, 16, 16, 16);
            tableRow.addView(priceCell);

            orderedProducts.addView(tableRow);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.alert_conf_heading);
        builder.setView(layout);
        builder.setPositiveButton(R.string.alert_conf_order, (dialog, id) ->
                listener.accept()
        );

        builder.setNegativeButton(R.string.alert_cancel, (dialog, id) ->
                listener.cancel()
        );

        return builder.create();
    }

    public interface ConfirmationResponseListener {
        void accept();

        void cancel();
    }
}
