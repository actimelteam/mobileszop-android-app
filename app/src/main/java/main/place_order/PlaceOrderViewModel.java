package main.place_order;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Pair;

import com.zpi.mobileshop.BuildConfig;
import com.zpi.mobileshop.R;

import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import extra.DateUtils;
import extra.GoogleSignInHelper;
import extra.SingleLiveEvent;
import extra.Tuple4;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import main.basket.BasketItem;
import main.basket.BasketViewModel;
import orders.api.IPlaceOrderAPI;
import orders.api.OrderPositionDto;
import orders.api.PlacedOrderDto;
import products.Product;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SuppressWarnings("WeakerAccess")
public class PlaceOrderViewModel extends AndroidViewModel {
    private final MutableLiveData<Void> needToSignInLiveData = new MutableLiveData<>();
    private final SingleLiveEvent<Void> orderDismissMutableLiveData = new SingleLiveEvent<>();
    private final MutableLiveData<OrderConfirmationDialog> orderConfirmationDialogMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> placingOrderInProgressLiveData = new MutableLiveData<>();
    private final SingleLiveEvent<Void> successLiveData = new SingleLiveEvent<>();
    private final MutableLiveData<Integer> failureLiveData = new MutableLiveData<>();
    private final SingleLiveEvent<Void> exceptionLiveData = new SingleLiveEvent<>();
    private final Realm realm;
    private final CompositeDisposable disposable = new CompositeDisposable();

    private BasketViewModel basketViewModel;

    public PlaceOrderViewModel(@NonNull Application application) {
        super(application);
        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onCleared() {
        disposable.clear();
        realm.close();
        super.onCleared();
    }

    public OrderTimePicker openTimePicker(Context context, BasketViewModel basketViewModel) {
        this.basketViewModel = basketViewModel;

        if (!GoogleSignInHelper.getInstance().isSignedIn()) {
            needToSignInLiveData.postValue(null);
            return null;
        }

        Bundle arguments = new Bundle();
        arguments.putShortArray("TIME_RANGE_FROM", new short[]{16, 0});
        arguments.putShortArray("TIME_RANGE_TO", new short[]{21, 0});

        OrderTimePicker pickerDialog = new OrderTimePicker();
        pickerDialog.setArguments(arguments);
        pickerDialog.setListener(new OrderTimePicker.TimePickerResponseListener() {
            @Override
            public void accept(GregorianCalendar collectionDate) {
                showConfirmation(context, collectionDate);
            }

            @Override
            public void cancel() {
                orderDismissMutableLiveData.call();
            }
        });

        return pickerDialog;
    }

    private void showConfirmation(Context context, GregorianCalendar collectionDate) {
        disposable.add(
                prepareOrderDtoAndTableData(context, collectionDate).subscribe(data -> {
                    OrderConfirmationDialog confirmationDialog = new OrderConfirmationDialog();
                    PlacedOrderDto placedOrderDto = data.first;
                    String totalSumString = String.format(Locale.getDefault(), context.getString(R.string.subtotal_string_currency), basketViewModel.getSubTotalLiveData().getValue());

                    List<Tuple4<String, String, String, String>> tableData = data.second;

                    confirmationDialog.setClientName(GoogleSignInHelper.getInstance().getCurrentAccount().getDisplayName());
                    confirmationDialog.setSubmissionDateString(DateUtils.formatDateYMDHM(DateUtils.getCurrentDate()));
                    confirmationDialog.setPlannedCollectionDateString(DateUtils.formatDateYMDHM(collectionDate.getTime()));
                    confirmationDialog.setTotalSumString(totalSumString);
                    confirmationDialog.setTableData(tableData);

                    confirmationDialog.setListener(new OrderConfirmationDialog.ConfirmationResponseListener() {
                        @Override
                        public void accept() {
                            placingOrderInProgressLiveData.postValue(true);
                            startPlacingOrder(placedOrderDto);
                        }

                        @Override
                        public void cancel() {
                            orderDismissMutableLiveData.call();
                        }
                    });

                    orderConfirmationDialogMutableLiveData.postValue(confirmationDialog);
                })
        );
    }

    public void startPlacingOrder(PlacedOrderDto placedOrderDto) {
        disposable.add(
                Observable.fromCallable(() -> {
                    IPlaceOrderAPI placeOrderAPI = new Retrofit.Builder()
                            .baseUrl(BuildConfig.WebAPIAddress)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build()
                            .create(IPlaceOrderAPI.class);

                    Call<Void> call = placeOrderAPI.placeOrder(placedOrderDto);
                    return call.execute();
                })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                response -> {
                                    placingOrderInProgressLiveData.postValue(false);

                                    if (response.isSuccessful()) {
                                        basketViewModel.removeAllItemsAsync();
                                        successLiveData.call();

                                        Log.v("OrderResult", "Success! Order has been placed.");
                                    } else {
                                        failureLiveData.postValue(response.code());
                                        Log.e("PlaceOrderResult", "Failure. Error code: " + response.code());
                                    }
                                }
                                , throwable -> {
                                    Log.e("PlaceOrderResult", "Exception: " + throwable.getMessage());
                                    throwable.printStackTrace();
                                    exceptionLiveData.call();
                                })
        );
    }

    private Observable<Pair<PlacedOrderDto, List<Tuple4<String, String, String, String>>>> prepareOrderDtoAndTableData(Context context, GregorianCalendar collectionDate) {
        return Observable.fromCallable(() -> {
            Realm realm = Realm.getDefaultInstance();
            LinkedList<OrderPositionDto> orderPositions = new LinkedList<>();
            String token;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
            String submissionDateString = dateFormat.format(new Date());
            String collectionDateString = dateFormat.format(collectionDate.getTime());

            PlacedOrderDto placedOrderDto;
            LinkedList<Tuple4<String, String, String, String>> tableData = new LinkedList<>();

            realm.executeTransaction(realmInst -> {
                RealmResults<BasketItem> items = realmInst.where(BasketItem.class).findAll();

                for (BasketItem basketItem : items) {
                    Product product = basketItem.getProduct();

                    orderPositions.add(new OrderPositionDto(
                            basketItem.getProductId(),
                            basketItem.getAmount(),
                            product.getPrice())
                    );

                    tableData.add(new Tuple4<>(
                            product.getName(),
                            product.getPriceString(context),
                            basketItem.getAmountString(context),
                            basketItem.getPriceString(context))
                    );
                }
            });

            realm.close();

            token = GoogleSignInHelper.getInstance().getCurrentAccount().getIdToken();
            if (token == null) {
                throw new InvalidParameterException("Not signed in to Google.");
            }

            placedOrderDto = new PlacedOrderDto(token, orderPositions, submissionDateString, collectionDateString);
            return new Pair<>(placedOrderDto, (List<Tuple4<String, String, String, String>>) tableData);
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public MutableLiveData<Void> getNeedToSignInLiveData() {
        return needToSignInLiveData;
    }

    public SingleLiveEvent<Void> getOrderDismissMutableLiveData() {
        return orderDismissMutableLiveData;
    }

    public MutableLiveData<OrderConfirmationDialog> getOrderConfirmationDialogMutableLiveData() {
        return orderConfirmationDialogMutableLiveData;
    }

    public MutableLiveData<Boolean> getPlacingOrderInProgressLiveData() {
        return placingOrderInProgressLiveData;
    }

    public SingleLiveEvent<Void> getSuccessLiveData() {
        return successLiveData;
    }

    public MutableLiveData<Integer> getFailureLiveData() {
        return failureLiveData;
    }

    public SingleLiveEvent<Void> getExceptionLiveData() {
        return exceptionLiveData;
    }
}
