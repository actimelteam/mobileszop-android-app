package main.search;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.zpi.mobileshop.databinding.ActivityBaseSearchResultBinding;

class SearchResultViewHolder extends RecyclerView.ViewHolder {
    private final ActivityBaseSearchResultBinding binding;

    SearchResultViewHolder(ActivityBaseSearchResultBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    void bind(@NonNull SearchResult item) {
        binding.setResult(item);
        binding.executePendingBindings();
    }
}
