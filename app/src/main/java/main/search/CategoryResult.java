package main.search;

import android.content.Context;
import android.text.Spannable;

import com.zpi.mobileshop.R;

import categories.Category;
import extra.StringUtils;

public class CategoryResult extends SearchResult {
    private final Category category;
    private final SearchViewModel searchViewModel;
    private final String[] searchCriteria;

    CategoryResult(Category category, SearchViewModel searchViewModel, String[] searchCriteria) {
        this.category = category;
        this.searchViewModel = searchViewModel;
        this.searchCriteria = searchCriteria;
    }

    @Override
    public Spannable getTitle() {
        return StringUtils.makeWordsInStringBold(category.getName(), searchCriteria);
    }

    @Override
    public String getOrigin(Context context) {
        return context.getString(R.string.search_result_from_categories);
    }

    @Override
    public void open() {
        searchViewModel.openSearchResult(this);
    }

    @Override
    public Object getObject() {
        return category;
    }

    @Override
    public boolean equals(SearchResult otherSearchResult) {
        return getTitle().equals(otherSearchResult.getTitle()) && getClass() == otherSearchResult.getClass();
    }
}
