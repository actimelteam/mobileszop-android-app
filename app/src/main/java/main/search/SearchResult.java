package main.search;

import android.content.Context;
import android.text.Spannable;

@SuppressWarnings("WeakerAccess")
public abstract class SearchResult {
    public abstract Spannable getTitle();

    public abstract String getOrigin(Context context);

    public abstract void open();

    public abstract Object getObject();

    public abstract boolean equals(SearchResult otherSearchResult);
}
