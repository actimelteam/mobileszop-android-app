package main.search;

import android.content.Context;
import android.text.Spannable;

import com.zpi.mobileshop.R;

import extra.StringUtils;
import products.Product;

public class ProductResult extends SearchResult {
    private final Product product;
    private final SearchViewModel searchViewModel;
    private final String[] searchCriteria;

    ProductResult(Product product, SearchViewModel searchViewModel, String[] searchCriteria) {
        this.product = product;
        this.searchViewModel = searchViewModel;
        this.searchCriteria = searchCriteria;
    }

    @Override
    public Spannable getTitle() {
        return StringUtils.makeWordsInStringBold(product.getName(), searchCriteria);
    }

    @Override
    public String getOrigin(Context context) {
        return context.getString(R.string.search_result_from_products);
    }

    @Override
    public void open() {
        searchViewModel.openSearchResult(this);
    }

    @Override
    public Object getObject() {
        return product;
    }

    @Override
    public boolean equals(SearchResult otherSearchResult) {
        return getTitle().equals(otherSearchResult.getTitle()) && getClass() == otherSearchResult.getClass();
    }
}
