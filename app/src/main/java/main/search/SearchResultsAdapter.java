package main.search;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zpi.mobileshop.R;
import com.zpi.mobileshop.databinding.ActivityBaseSearchResultBinding;

public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultViewHolder> {
    private SearchResult[] data;

    public SearchResultsAdapter() {
        data = new SearchResult[0];
    }

    public void updateData(SearchResult[] newData) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffSearchResultComparator(data, newData));
        data = newData;
        diffResult.dispatchUpdatesTo(this);
    }

    @NonNull
    @Override
    public SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ActivityBaseSearchResultBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.activity_base_search_result, parent, false);
        return new SearchResultViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultViewHolder holder, int position) {
        holder.bind(data[position]);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }
}
