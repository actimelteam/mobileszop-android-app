package main.search;

import android.support.v7.util.DiffUtil;

class DiffSearchResultComparator extends DiffUtil.Callback {
    private final SearchResult[] oldList;
    private final SearchResult[] newList;

    DiffSearchResultComparator(SearchResult[] oldList, SearchResult[] newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.length;
    }

    @Override
    public int getNewListSize() {
        return newList.length;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList[oldItemPosition] == newList[newItemPosition];
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList[oldItemPosition].equals(newList[newItemPosition]);
    }
}
