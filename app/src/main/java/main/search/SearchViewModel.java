package main.search;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicReference;

import categories.Category;
import categories.api.CategoryService;
import io.realm.Realm;
import io.realm.RealmResults;
import products.Product;
import products.api.ProductsService;

public class SearchViewModel extends AndroidViewModel {
    private final Realm realm;
    private final MutableLiveData<LinkedList<SearchResult>> searchResultLiveData = new MutableLiveData<>();
    private final MutableLiveData<SearchResult> openSearchResultLiveData = new MutableLiveData<>();

    public SearchViewModel(@NonNull Application application) {
        super(application);
        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onCleared() {
        realm.close();
        super.onCleared();
    }

    public void updateSearchResults(String searchQuery) {
        if (!searchQuery.trim().isEmpty()) {
            String[] criteria = searchQuery.split(" ");

            RealmResults<Product> resultProducts = ProductsService.findProductsLike(realm, criteria);
            RealmResults<Category> resultCategories = CategoryService.findCategoriesLike(realm, criteria);
            LinkedList<SearchResult> addResults = new LinkedList<>();
            AtomicReference<Short> amountOfData = new AtomicReference<>((short) 0);

            resultProducts.addChangeListener(products -> {
                for (Product product : products) {
                    addResults.add(new ProductResult(product, this, criteria));
                }

                if (amountOfData.get() == 1) {
                    searchResultLiveData.postValue(addResults);
                } else {
                    amountOfData.getAndSet((short) (amountOfData.get() + 1));
                }

                resultProducts.removeAllChangeListeners();
            });

            resultCategories.addChangeListener(categories -> {
                for (Category category : categories) {
                    addResults.add(new CategoryResult(category, this, criteria));
                }

                if (amountOfData.get() == 1) {
                    searchResultLiveData.postValue(addResults);
                } else {
                    amountOfData.getAndSet((short) (amountOfData.get() + 1));
                }

                resultCategories.removeAllChangeListeners();
            });
        } else {
            searchResultLiveData.postValue(null);
        }
    }

    public void openSearchResult(SearchResult searchResult) {
        openSearchResultLiveData.postValue(searchResult);
    }

    public MutableLiveData<LinkedList<SearchResult>> getSearchResultLiveData() {
        return searchResultLiveData;
    }

    public MutableLiveData<SearchResult> getOpenSearchResultLiveData() {
        return openSearchResultLiveData;
    }
}
