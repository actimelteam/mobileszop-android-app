package main;

import android.app.Application;

import extra.GoogleSignInHelper;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainApplication extends Application {
    public void onCreate() {
        super.onCreate();
        Realm.init(this);

        Realm.setDefaultConfiguration(new RealmConfiguration.Builder()
                .name("database")
                .build()
        );

        GoogleSignInHelper.getInstance().prepare(this);
    }
}
