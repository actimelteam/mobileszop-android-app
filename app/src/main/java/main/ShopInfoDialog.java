package main;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;

import com.zpi.mobileshop.R;

import java.security.InvalidParameterException;
import java.util.Locale;

import splash.api.ShopData;

public class ShopInfoDialog extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        FragmentActivity activity = getActivity();
        ShopData shopData = ShopData.getInstance();

        if (activity == null) {
            throw new InvalidParameterException("Activity is null");
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        String text = String.format(
                Locale.getDefault(),
                getString(R.string.dialog_about_shop_content),
                shopData.getShopName(),
                shopData.getAddress(),
                shopData.getAdditionalInfo(),
                String.valueOf(shopData.getEndOrderingTime()),
                String.valueOf(shopData.getStartCollectingTime()),
                String.valueOf(shopData.getEndCollectingTime())
        );

        builder.setMessage(text);
        builder.setPositiveButton(R.string.alert_ok, null);
        return builder.create();

    }
}
