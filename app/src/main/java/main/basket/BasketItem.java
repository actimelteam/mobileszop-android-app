package main.basket;

import android.content.Context;
import android.support.annotation.NonNull;

import com.zpi.mobileshop.R;

import java.util.Locale;
import java.util.Objects;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import products.Product;

public class BasketItem extends RealmObject implements Comparable<BasketItem> {
    @SuppressWarnings("unused")
    @PrimaryKey
    private long productId;

    private Product product;

    private float amount;

    public BasketItem() {
    }

    public BasketItem(Product product, float amount) {
        this.productId = product.getId();
        this.product = product;
        this.amount = amount;
    }

    public long getProductId() {
        return productId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public float getAmount() {
        return amount;
    }

    public String getAmountString(Context context) {
        switch (product.getUnit()) {
            case KILOGRAM:
                return String.format(Locale.getDefault(), context.getString(R.string.amount_kg), amount);

            case GRAM:
                return String.format(Locale.getDefault(), context.getString(R.string.amount_g), amount);

            case PIECE:
                return String.format(Locale.getDefault(), context.getString(R.string.amount_pcs), amount);

            case LITER:
                return String.format(Locale.getDefault(), context.getString(R.string.amount_ltr), amount);
        }

        return String.format(Locale.getDefault(), context.getString(R.string.amount_pcs), amount);
    }

    @SuppressWarnings("WeakerAccess")
    public void setAmount(float amount) {
        this.amount = amount;

        if (amount <= 0F) {
            deleteFromRealm();
        }
    }

    public double getPrice() {
        return product.getPrice() * amount;
    }

    public String getPriceString(Context context) {
        return String.format(Locale.getDefault(), context.getString(R.string.subtotal_string_currency), product.getPrice() * amount);
    }

    public void addAmount(float amountToAdd) {
        if (amount + amountToAdd >= 0) {
            setAmount(amount + amountToAdd);
        } else {
            removeItem();
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void removeItem() {
        deleteFromRealm();
    }

    @Override
    public int hashCode() {
        return product.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasketItem that = (BasketItem) o;
        return Float.compare(that.amount, amount) == 0 &&
                Objects.equals(product, that.product);
    }

    @Override
    public int compareTo(@NonNull BasketItem basketItem) {
        return Float.compare(amount, basketItem.amount);
    }

    @Override
    public String toString() {
        return "BasketItem{" +
                "product=" + product +
                ", amount=" + amount +
                '}';
    }
}
