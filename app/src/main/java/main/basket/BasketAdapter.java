package main.basket;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zpi.mobileshop.R;
import com.zpi.mobileshop.databinding.ActivityBaseBasketItemBinding;

import io.realm.RealmRecyclerViewAdapter;
import products.ProductDialogFragment;


public class BasketAdapter extends RealmRecyclerViewAdapter<BasketItem, BasketItemViewHolder> {
    private final AppCompatActivity activity;
    private final BasketViewModel basketViewModel;

    public BasketAdapter(AppCompatActivity activity, BasketViewModel basketViewModel) {
        super(basketViewModel.getBasketItemsLiveData().getResults(), true, true);
        this.activity = activity;
        this.basketViewModel = basketViewModel;
    }

    @NonNull
    @Override
    public BasketItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ActivityBaseBasketItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.activity_base_basket_item, parent, false);
        return new BasketItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BasketItemViewHolder holder, int position) {
        BasketItem item = getItem(position);

        if (item != null) {
            holder.bind(item, basketViewModel);

            holder.setOnClickListener(view -> {
                ProductDialogFragment dialog = new ProductDialogFragment();
                dialog.setParameters(item.getProduct(), basketViewModel);
                dialog.show(activity.getSupportFragmentManager(), "PRODUCT");
            });
        }
    }
}
