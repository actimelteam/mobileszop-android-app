package main.basket;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.zpi.mobileshop.databinding.ActivityBaseBasketItemBinding;

public class BasketItemViewHolder extends RecyclerView.ViewHolder {
    private final ActivityBaseBasketItemBinding binding;

    BasketItemViewHolder(ActivityBaseBasketItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    void bind(@NonNull BasketItem item, BasketViewModel basketViewModel) {
        binding.setItem(item);
        binding.setViewModel(basketViewModel);
        binding.executePendingBindings();
    }

    void setOnClickListener(View.OnClickListener listener) {
        binding.getRoot().setOnClickListener(listener);
    }
}
