package main.basket;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import extra.RealmLiveData;
import extra.SingleLiveEvent;
import io.realm.Realm;
import products.Product;

public class BasketViewModel extends AndroidViewModel {
    private final Realm realm;
    private final MutableLiveData<Float> subTotalLiveData = new MutableLiveData<>();
    private final RealmLiveData<BasketItem> basketItemsLiveData;
    private final SingleLiveEvent<Void> productDialogRefreshLiveEvent = new SingleLiveEvent<>();

    public BasketViewModel(@NonNull Application application) {
        super(application);
        realm = Realm.getDefaultInstance();

        basketItemsLiveData = new RealmLiveData<>(
                realm
                        .where(BasketItem.class)
                        .findAllAsync()
        );

        basketItemsLiveData.addChangeListener((basketItems, changeSet) -> {
            float result = 0F;

            for (BasketItem item : basketItems) {
                result += item.getPrice();
            }

            subTotalLiveData.setValue(result);
        });
    }

    @Override
    protected void onCleared() {
        realm.close();
        super.onCleared();
    }

    @SuppressWarnings("WeakerAccess")
    public void addProductAmount(long productId, float amountToAdd) {
        realm.executeTransactionAsync(realmInst -> {
            Product product = realmInst
                    .where(Product.class)
                    .equalTo("id", productId)
                    .findFirst();

            assert product != null;
            BasketItem basketItem = product.getBasketItem();

            if (basketItem == null) {
                basketItem = realmInst.createObject(BasketItem.class, productId);
                basketItem.setProduct(product);
            }

            basketItem.addAmount(amountToAdd);
            product.setDescription(product.getDescription());
        }, productDialogRefreshLiveEvent::call); //onSuccess
    }

    public void removeProduct(long productId) {
        addProductAmount(productId, -Float.MAX_VALUE);
    }

    public void removeAllItemsAsync() {
        realm.executeTransactionAsync(realmInst ->
                realmInst.where(BasketItem.class).findAll().deleteAllFromRealm()
        );
    }

    public MutableLiveData<Float> getSubTotalLiveData() {
        return subTotalLiveData;
    }

    public RealmLiveData<BasketItem> getBasketItemsLiveData() {
        return basketItemsLiveData;
    }

    public SingleLiveEvent<Void> getProductDialogRefreshLiveEvent() {
        return productDialogRefreshLiveEvent;
    }
}
